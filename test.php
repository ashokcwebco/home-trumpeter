<meta name="_token" content="{{csrf_token()}}" />
        <!-- slider -->
        <div class="clearfix"></div>
        <div class="video">
            <div class="clearfix"></div>
            <div class="headerbg">
                <div class="col-md-12" align="center"><h1>Send Invitation</h1></div>
            </div>
            <div class="container">
                <div class="height30"></div>
                <div class="alert alert-success" style="display:none">
                </div>
                <div class="alert alert-danger" style="display:none">
                </div>
                <div class="container">
                   
                    <div class="clearfix"></div>
                    <div class="row" align="right" style="margin-bottom:2px;">
                    <a href="javascript:;" id="invite" class="btn btn-primary radiusoff">Invite</a>
                    </div>
                    <input type="hidden" id="proposalId" value="{{$proposal}}" > 
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="checkAll"></th>
                                        <th>Service Provider</th>
                                        <th>Mob. Number</th>
                                        <th>Service Time (open/close)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($shops as $key=>$shop)
                                        <tr>
                                            <th><input type="checkbox" name="shop[]" value="{{$shop->id}}" ></th>
                                            <td>{{$shop->shop_name }}</td>
                                            <td>{{$shop->shop_phone_no }} </td>
                                            <td>{{ $shop->start_time.':00 / '. $shop->end_time.':00' }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>