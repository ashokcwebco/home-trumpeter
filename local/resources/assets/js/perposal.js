function getServices(service_id){
    var xhttp = new XMLHttpRequest();
    var url = "getsubservice";
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          var  obj = JSON.parse(this.response);

          var select = document.createElement("select");
          select.multiple = true;
          select.setAttribute("id", "langOpt");
          select.setAttribute("name", "langOpt[]");
          select.setAttribute("class", "validate[required]");
          for(var i=0; i < obj.length; i++){
            var option = document.createElement("option");
            option.text = obj[i].subname;
            option.value = obj[i].subid;
            select.appendChild(option);
          }
          var d1 = document.getElementById('select');
          d1.innerHTML = "";
          d1.appendChild(select);
      }
    };
    xhttp.open("GET", url+"/"+service_id, true);
    xhttp.send();
}