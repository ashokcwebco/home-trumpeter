<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
        <?php $url = URL::to("/"); ?>
        <!-- fixed navigation bar -->
        <script src="<?php echo $url;?>/js/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
        @include('header')
        <!-- slider -->
       
        <div class="clearfix"></div>
        <div class="video">
            <div class="clearfix"></div>
            <div class="container">
                <h1>Create Proposal</h1>
                <div class="clearfix"></div>
                @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
                @endif
                <?php $url = URL::to("/"); ?>
                <form class="form-horizontal" role="form" method="POST" action="{{ route('addperposal') }}" id="formID" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row profile shop">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12">Job Title <span class="require">*</span></label>
                                <div class="col-md-12">
                                    <input id="title" type="text" class="form-control validate[required] text-input" name="title" value="">
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12">Location <span class="require">*</span></label>
                                <div class="col-md-12">
                                    <input id="title" type="text" class="form-control validate[required] text-input" name="location" value="">
                                    @if ($errors->has('location'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('location') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12">Start Date <span class="require">*</span></label>
                                <div class="col-md-12">
                                <input type="text" class="form-control validate[required]" id="startDate" name="startDate" > 
                                    @if ($errors->has('location'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('location') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-md-12">End Date <span class="require">*</span></label>
                                <div class="col-md-12">
                                <input type="text" class="form-control validate[required]" id="endDate" name="endDate" > 
                                    @if ($errors->has('location'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('location') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 moves20">
                           <div class="form-group">
                                <label for="name" class="col-md-12">Services <span class="require">*</span></label>
                                <div class="col-md-12 swidth noborder" id="select">
                                    <select name="services[]" multiple id="langOpt" class=" validate[required]">
                                        @foreach($subservices as $service)
                                            <option value="{{$service->subid}}">{{$service->subname}}</option>
                                        @endforeach;
                                    </select>
                                    @if ($errors->has('services'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('services') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="form-group">
                            <label for="name" class="col-md-12">Description <span class="require">*</span></label>
                            <div class="col-md-12">
                                <textarea name="description"  rows="12" class="form-control validate[required]" ></textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success radiusoff">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                var  selectedDate = '';
                 $('#startDate').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    minDate: 1,
                    beforeShowDay: function (date) {
                        var day = date.getDay();
                        return [('day==0||day==1||day==2||day==3||day==4||day==5||day==6')];
                    },
                    onSelect:function(date){
                        selectedDate = date
                        $('#endDate').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            minDate: 2,
                            beforeShowDay: function (date) {
                                var day = date.getDay();
                                return [('day==1||day==2||day==3||day==4||day==5')];
                            }
                        });
                        var date = new Date(selectedDate);
                        var currentMonth1 = date.getMonth();
                        var currentDate1 = date.getDate();
                        var currentYear1 = date.getFullYear();
                        $("#endDate").datepicker( "option", "minDate", new Date(currentYear1, currentMonth1, currentDate1));
                    }
                });
                var date = new Date();
                var currentMonth = date.getMonth();
                var currentDate = date.getDate();
                var currentYear = date.getFullYear();
                $("#startDate").datepicker( "option", "minDate", new Date(currentYear, currentMonth, currentDate));

    
            });   

        </script>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        @include('footer')
    </body>
</html>