<!DOCTYPE html>
<html lang="en">
    <head>
    </head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        .page-break {
            page-break-after: always;
        }
    </style>
    <body style="width : 1175px;">
        <?php $url = URL::to("/"); ?>
        <!-- fixed navigation bar -->
        <meta name="_token" content="{{csrf_token()}}" />
        <!-- slider -->
        <div class="clearfix"></div>
        <link href="{{URL::to('/css/stylish-portfolio.css')}}" rel="stylesheet">
        <div class="approval-res">
            <div class="top-sec container-fluid header">
                <div class="row">
                    <div class="col-md-12 logo-sec">
                        <span class="coll-3 input-effect"><img src="{{URL::to('/img/logo-l.png')}}"></span>
                        <div class="logo-cnt">
                            <span class="coll-3 input-effect" style="float : left;width : 100%;text-align:right;"><input class="effect-18" type="text" value="{{$postdata->header_service_provider}}" placeholder="Service Provider Number/Name"><span class="focus-border"></span></span>
                        </div>
                    </div>
                    <div class="col-md-12 res-sec">
                        <h1>Response to Proposal</h1>
                        <div class="col-md-12 res-sec1">
                            <h3 style="float: left; width:80% text-align:rigth;">Project Name:</h3>
                            <span class="coll-3 input-effect" style="float: left; width:20% "><input class="effect-18" type="text" value="{{$postdata->header_project_name}}" placeholder="Project Name"><span class="focus-border"></span></span>
                        </div>
                    </div>
                    <div class="col-md-12 res-sec">
                        <h1></h1>
                        <div class="col-md-12 res-sec1">
                            <h3 style="float: left; width:80% text-align:rigth;">Prepared for:</h3> 
                            <span class="" style="float:left ; width:20%" ><input class="effect-18" type="text" value="{{$postdata->header_customer_name}}" placeholder="Customer Name"><span class="focus-border"></span></span>
                        </div>
                        <p class="date top-date">Date: 08/08/2018</p>
                    </div>
                </div>
            </div>
            <div class="main-cnt-sec container-fluid">
                <div class="row">
                    <div class="col-md-12 overview-sec">
                        <h2>Overview of <span class="coll-3 input-effect"><input class="effect-18" class="effect-18" type="text" value="{{$postdata->over_service_provider_title}}" placeholder="Service Provider"><span class="focus-border"></span></span></h2>
                        <p>Thank you for connecting with <span class="coll-3 input-effect"><span class="coll-3 input-effect"><input class="effect-18" class="effect-18" type="text" value="{{$postdata->over_service_provider}}" placeholder="Service Provider Name"><span class="focus-border"></span></span>for your installation/repairs project needs. 
                                We specialize in <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->over_domain_area}}" placeholder="Domain Area"><span class="focus-border"></span></span> servicing clients around <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->over_city_zip}}" placeholder="Zip Code City"><span class="focus-border"></span></span> area. We pride ourselves in following best practices and methodology delivering <span class="coll-3 input-effect"><input class="effect-18" type="text"  value="{{$postdata->over_service_provider2}}" placeholder="Service Provider Name"><span class="focus-border"></span></span> related services. We believe we can make a difference and provide solution for <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->over_customer}}" placeholder="customer"><span class="focus-border"></span></span> @ <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->over_address}}" placeholder="Address Location"><span class="focus-border"></span></span>  with the best cost saving and quality service if you select us for your project</p>
                    </div>
                    <div class="col-md-12 Assessment-sec">
                        <h2>Assessment</h2>
                        <p>As an industry leader within the <span class="coll-3 input-effect"><input class="effect-18" value="{{$postdata->assessment_domain}}" placeholder="Domain" type="text"><span class="focus-border"></span></span>, <span class="coll-3 input-effect"><input class="effect-18"  value="{{$postdata->assessment_service_provider1}}" placeholder="Service Provider/Profile" type="text"><span class="focus-border"></span></span> provides qualified professionals to tackle <span class="coll-3 input-effect"><input class="effect-18" type="text"  value="{{$postdata->assessment_customer}}" placeholder="customer"><span class="focus-border"></span></span>challenges and resolve any problem with various task(s) listed in your <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->assessment_project_name}}" placeholder="project name"> <span class="focus-border"></span></span>project. We are available for site assessment and review of more details you may not have covered in your initial request for proposal. <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->assessment_service_provider2}}" placeholder="Service provider"><span class="focus-border"></span></span> guarantee success in your project because we are:</p>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Requirement</th>
                                        <th>Response</th>
                                        <th>Comment</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Covered by Insurance/Insured</td>
                                        <td>
                                            <label class="switch">
                                                {{isset($postdata->Insured) && $postdata->Insured == 'on'?'Y':'N'}}
                                            </label>
                                        </td>
                                        <td>
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->Insured_comment}}" placeholder=""><span class="focus-border"></span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Availability of resource and Equipment to deliver</td>
                                        <td>
                                            <label class="switch">
                                                {{isset($postdata->deliver) && $postdata->deliver == 'on'?'Y':'N'}}
                                            </label>
                                        </td>
                                        <td>
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->deliver_comment}}"  placeholder=""><span class="focus-border"></span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Available for Interview </td>
                                        <td>
                                            <label class="switch">
                                                {{isset($postdata->interview) && $postdata->interview == 'on'?'Y':'N'}}
                                            </label>
                                        </td>
                                        <td>
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->interview_comment}}"  placeholder=""><span class="focus-border"></span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Licensed and Bonded     </td>
                                        <td>
                                            <label class="switch">
                                                {{isset($postdata->bonded) && $postdata->bonded == 'on'?'Y':'N'}}
                                            </label>
                                        </td>
                                        <td>
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text"  value="{{$postdata->bonded_comment}}" placeholder=""><span class="focus-border"></span></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Previous job reference</td>
                                        <td>
                                            <label class="switch">
                                                {{isset($postdata->job_reference) && $postdata->job_reference == 'on'?'Y':'N'}}
                                            </label>							</td>
                                        <td>Protect Customer privacy</td>
                                    </tr>
                                    <tr>
                                        <td>Community Support hours</td>
                                        <td>
                                            <label class="switch">
                                                {{isset($postdata->support_hrs) && $postdata->support_hrs == 'on'?'Y':'N'}}
                                            </label>
                                        </td>
                                        <td>60 hours</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 Implementationnt-sec">
                        <h2>Implementation and Execution Strategy</h2>
                        <p>Our execution strategy incorporates proven methodologies, extremely qualified personnel, and a highly responsive approach to managing deliverables. We guarantee daily/weekly updates depending on mutual agreement between  <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->company_name}}" placeholder="Service provider company"><span class="focus-border"></span></span> and <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->stategy_customer_name}}" placeholder="Customer"><span class="focus-border"></span></span></p>
                    </div>
                    <div class="col-md-12 Expected-sec">
                        <h2>Expected Result</h2>
                        <p>After complete delivery of this project  <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->result_service_provider1}}" placeholder="service provider"><span class="focus-border"></span></span> guarantees to meet the expectations of <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->result_customer1}}" placeholder="Customer"><span class="focus-border"></span></span> within the framework of the requirement for this <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->result_project_name1}}" placeholder="project name"><span class="focus-border"></span></span></p>
                    </div>
                    <div class="col-md-12 Project-sec">
                        <h2>Project Team</h2>
                        <p>Our delivery team is based out <span class="coll-3 input-effect"><input class="effect-18" type="text"  value="{{$postdata->team_city_zip}}" placeholder="Zip code City"><span class="focus-border"></span></span>. Our team member are always available to address your ongoing project concerns or send us a message through Home trumpeter chat room/email etc…</p>
                    </div>
                    <div class="col-md-12 PDeliverables-sec">
                        <h2>Project Deliverables</h2>
                        <p><span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->deliverable_service_provider}}" placeholder="Service provider"><span class="focus-border"></span></span>will deliver this project within <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{date('d M Y', strtotime($postdata->deliverable_start_date))}}" placeholder="Proposed Start Date"><span class="focus-border"></span></span> and <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{date('d M Y', strtotime($postdata->deliverable_end_date))}}" placeholder="proposed end date"><span class="focus-border"></span></span> These dates are best guess estimates and are subject to change under a situation or circumstances beyond the control of the <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->deliverable_service_provider2}}" placeholder="service provider"><span class="focus-border"></span></span> and <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->deliverable_customer}}" placeholder="customer"><span class="focus-border"></span></span>.One of such situations include inclement weather condition. The project deliverable includes the following tasks:</p>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Task</th>
                                        <th>Description</th>
                                        <th>Estimate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->list_desc1}}" placeholder="Items listed by the Customer"><span class="focus-border"></span></span>
                                        </td>
                                        <td>
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text"  value="{{$postdata->list_estimate1}}" placeholder=""><span class="focus-border"></span></span>	
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->list_desc2}}"  placeholder="Items listed by the Customer"><span class="focus-border"></span></span>
                                        </td>
                                        <td>
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->list_estimate2}}" placeholder=""><span class="focus-border"></span></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 Pricing-sec">
                        <h2>Pricing and Billing:</h2>
                        <p><span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->billing_service_provider1}}" placeholder="Service provider"><span class="focus-border"></span></span> collect payment through HomeTrumpeter platform.  We charge our client <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->billing_hrs}}" placeholder="/hr"><span class="focus-border"></span></span>.<span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->billing_customer_name}}" placeholder="Customer"><span class="focus-border"></span></span> will initiate payment after satisfactory inspection of service as agreed by the two parties The rate listed above is the estimate for the service discussed. Estimates are subject to change if additional tasks are required before or during project execution. The cost associated with materials and parts shall be discussed before the commencement of the project.</p>
                    </div>
                    <div class="col-md-12 terms-sec">
                        <h2>Terms and Agreement</h2>
                        <p>Prior to any contractual agreement, the content of this proposals is subject to change. This change is a joint effort with mutual understanding between <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->agreement_customer}}" placeholder="customer"><span class="focus-border"></span></span>  and <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->agreement_service_provider}}" placeholder="Service provider"><span class="focus-border"></span></span></p>
                    </div>
                    <div class="col-md-12 Acceptance-sec">
                        <h2>Acceptance:</h2>
                        <div class="Acceptance-input">
                            <div class="inpt-sec">
                                <label>Name:</label>
                                <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->acceptance_customer_name}}" placeholder="Customer"><span class="focus-border"></span></span>
                            </div>
                        </div>
                        <div class="Acceptance-input">
                            <div class="inpt-sec">
                                <span class="coll-3 input-effect"><input class="effect-18" type="text" value="{{$postdata->acceptance_service_provider}}" placeholder="Service Provider"><span class="focus-border"></span></span>
                            </div>
                        </div>
                        <div class="Acceptance-input">
                            <div class="inpt-sec">
                                <label>Date:</label>
                                <input type="text" id="datepicker" value="{{date('d M Y', strtotime($postdata->date))}}"><input type="text" id="datepickerr">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 Acceptance-sec">
                    <h2></h2>
                        <div class="Acceptance-input">
                            <div class="inpt-sec">
                                <label>Date:</label>
                                <input type="text" id="datepicker" value="{{date('d M Y', strtotime($postdata->date))}}"><input type="text" id="datepickerr">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>