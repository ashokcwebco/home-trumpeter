 
<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
        <?php $url = URL::to("/"); ?>
        <!-- fixed navigation bar -->
        @include('header')
        <meta name="_token" content="{{csrf_token()}}" />
        <div class="heading_login">
            <h1>My Order</h1>    
        </div>	
        <div class="login_form">
            <div class="container">
                @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
                @endif
                @if(Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
                @endif
                <div class="dashboard">
                    <div class="col-md-12"> 
                        <div class="cart">
                            <div class="main_table_sec invita_nth">
                                <div class="table-responsive table_size_fix">
                                    <div class="scrol_fix_auto">
                                        <table class="table table-borderless">
                                            <thead>
                                                <tr>
                                                    <th>Job Title</th>
                                                    <th>Customer Name</th>
                                                    <th>Created At</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $initeIds = array(); ?>
                                                @foreach ($invitations as $key=>$invitation)
                                                <?php $initeIds[] = $invitation->invite_id; ?>
                                                <tr>
                                                    <td>{{$invitation->title }}</td>
                                                    <td>{{$invitation->senderfname.' '. $invitation->senderlname}} </td>
                                                    <td>{{ date('d M Y', strtotime($invitation->created_at))}}</td>
                                                    <td>
                                                        @if($invitation->status ==0)
                                                        <span style="color:#f2bc59;">Pending</span>
                                                        @elseif($invitation->status ==1)
                                                        <span style="color:#0DE50D;">Accepted</span>
                                                        @else
                                                        <span style="color:#CF2335;">Declined</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($invitation->status !=1)
                                                        <a href="{{URL::to('/single-rfp/'.$invitation->invite_id)}}" class="yellow_btn" target="blank">Accept</a>
                                                        <a href="{{URL::to('/invitation/decline/'.$invitation->invite_id)}}" class="red_btn" onclick="return confirm('Are you sure you want to decline this?')" >Decline</a>
                                                        @else
                                                        <a href="{{URL::to('/chat/'.$invitation->invite_id)}}" class="yellow_btn">Chat</a>
                                                        @endif  
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>   
                                </div>
                                <!-- <div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate"><a class="paginate_button previous disabled" aria-controls="myTable" data-dt-idx="0" tabindex="0" id="myTable_previous">Previous</a><span><a class="paginate_button current" aria-controls="myTable" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="3" tabindex="0">3</a></span><a class="paginate_button next" aria-controls="myTable" data-dt-idx="7" tabindex="0" id="myTable_next">Next</a><div></div></div> -->
                            </div>       
                        </div>
                    </div>
                </div> 
            </div>
        </div>    
        @include('footer')
    </body>
</html>