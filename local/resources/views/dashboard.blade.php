<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
        <!-- fixed navigation bar -->
        @include('header')
        <!-- slider -->
        <div class="heading_login">
            <h1>Dashboard</h1>    
        </div>	

        <div class="login_form">
            <div class="container">
                <div class="dashboard">
                    <div class="col-md-4"> 
                        <div class="cart">
                            <div class="col-md-12 profile_sec_side">
                                <center class="m-t-30">
                                    <div class="col-md-12">
                                        <div class="relativ_profile">
                                            <img src="{{($editprofile->photo)?URL::to('/local/images/userphoto/'.$editprofile->photo): URL::to('/local/images/nophoto.jpg')}}" class="" width="150">
                                        </div>  
                                        <h4 class="card-title m-t-10">{{$editprofile->name}}</h4>
                                        <span m-t-10>User Type : {{$editprofile->userTypeTitle}}</span>
                                    </div>    
                                    <div class="col-md-12">
                                        <a href="{{URL::to('/my_bookings')}}" class="au-btn au-btn-icon au-btn--blue m-l-20 sml login_btn">My Bookings</a>
                                    </div>
                                    <div class="col-md-12 account_sec">
                                        <ul>
                                            <li><a href="{{URL::to('/dashboard')}}"><i class="fa fa-user" aria-hidden="true"></i> Account Settings</a></li>
                                            <li><a href="{{URL::to('/delete-account')}}"><i class="fa fa-trash" aria-hidden="true"></i> Delete Account</a></li>
                                            <li><a href="{{URL::to('/logout')}}"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a></li>
                                        </ul>
                                    </div>
                                </center>
                            </div>    
                        </div>
                    </div>
                    <div class="col-md-8"> 
                        <div class="cart">
                            <div class="col-md-12">
                                @if(Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                                @endif
                                @if(Session::has('error'))
                                <div class="alert alert-danger">
                                    {{ Session::get('error') }}
                                </div>
                                @endif
                                <form class="form-horizontal" role="form" method="POST" action="{{ route('dashboard') }}" id="formID" enctype="multipart/form-data">
                                        {{ csrf_field() }}  
                                    <div class="lofin_fomr"> 
                                        <div class="custom_inp input-effect">
                                            <input id="name" type="text" class="effect-16 validate[required] text-input" name="name" value="{{$editprofile->name}}">
                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                            <label>Username</label>    
                                            <span class="focus-border"></span>
                                        </div>
                                        <div class="custom_inp input-effect">
                                            <input id="email" type="text" class="effect-16 validate[required,custom[email]] text-input" name="email" value="{{$editprofile->email}}">
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                            <label>E-Mail Address</label>    
                                            <span class="focus-border"></span>
                                        </div>
                                        <div class="custom_inp input-effect">
                                            <input id="password" type="password" class="effect-16"  name="password" value="">
                                            <label>Password</label>    
                                            <span class="focus-border"></span>
                                        </div>
                                        <input type="hidden" name="savepassword" value="{{$editprofile->password}}">
                                        <input type="hidden" name="id" value="{{$editprofile->id}}">
                                        <div class="custom_inp input-effect">
                                            <input id="phone" type="text" class="effect-16 validate[required] text-input" value="{{$editprofile->phone}}" name="phone">
                                            <label>Phone No</label>    
                                            <span class="focus-border"></span>
                                        </div>
                                        <div class="control-group">    
                                            <div class="select">
                                                <select name="gender" class=" validate[required] text-input">
                                                    <option value="">Gender</option>
                                                    <option value="male" <?php if ($editprofile->gender == 'male') { ?> selected="selected" <?php } ?>>Male</option>
                                                    <option value="female" <?php if ($editprofile->gender == 'female') { ?> selected="selected" <?php } ?>>Female</option>
                                                </select>
                                                <div class="select__arrow"></div>
                                            </div>
                                        </div>    
                                        <div class="custom_inp input-effect">
                                            <input type="file" id="photo" name="photo" class="effect-16">
                                            @if ($errors->has('photo'))
                                            <span class="help-block" style="color:red;">
                                                <strong>{{ $errors->first('photo') }}</strong>
                                            </span>
                                            @endif
                                            <label>Photo</label>    
                                            <span class="focus-border"></span>
                                        </div>
                                        <input type="hidden" name="currentphoto" value="{{$editprofile->photo}}">
                                        <input type="hidden" name="usertype" value="{{$editprofile->admin}}">
                                        <input type="submit" class="login_btn" style="background: #f6cd53;" value="Update">
                                    </div> 
                                </form>
                            </div> 
                            @if($editprofile->admin == 2)
                                <div class="col-md-12">
                                     <form class="form-horizontal" role="form" method="POST" action="{{ route('editproof') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <div class="lofin_fomr" style="padding-top: 0">    
                                            <div class="custom_inp input-effect">
                                                <input type="file" id="idProof" name="idProof" class="effect-16">
                                                <label>Id Proof</label>    
                                                <span class="focus-border"></span>
                                                <input type="hidden" name="currentIdProof" value="{{$proof->trade}}">
                                                <div class="col-md-6">
                                                    <img src="{{URL::to('/local/images/documents/'.$proof->trade)}}" class="img-responsive" hight="50" width="50" alt="">
                                                </div>
                                            </div>
                                            <div class="custom_inp input-effect">
                                                <input type="file" id="addressProof" name="addressProof" class="effect-16">
                                                <label>Address Proof</label>    
                                                <input type="hidden" name="currentAddressProof" value="{{$proof->proofInsurance}}>">
                                                <span class="focus-border"></span>
                                                <div class="col-md-6">
                                                    <img src="{{URL::to('/local/images/documents/'.$proof->proofInsurance)}}" class="img-responsive" hight="50" width="50" alt="">
                                                </div>
                                            </div>
                                            <input type="hidden" name="doc_id" value="{{isset($proof->doc_id)?$proof->doc_id:''}}">
                                            <div class="control-group">    
                                                <div class="select">
                                                    <select name="serviceType" class="effect-16" required>
                                                        <option value="">--Select Service--</option>
                                                        <option value="1" <?php echo $editprofile->service_type == 1?"selected='selected'":''; ?> >Individual</option>
                                                        <option value="2" <?php echo $editprofile->service_type == 2?"selected='selected'":''; ?> >Business</option>
                                                        <option value="3" <?php echo $editprofile->service_type == 3?"selected='selected'":''; ?>  >Both</option>
                                                    </select>
                                                    <div class="select__arrow"></div>
                                                </div>
                                            </div> 
                                            <input type="submit" class="login_btn" style="background: #f6cd53;" value="Submit"> 
                                        </div> 
                                    </form>
                                </div> 
                            @endif  
                        </div>
                    </div>
                </div> 
            </div>
        </div>   

        @include('footer')
    </body>
</html>