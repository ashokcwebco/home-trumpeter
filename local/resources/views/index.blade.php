<?php

use Illuminate\Support\Facades\Route;

$currentPaths = Route::getFacadeRoot()->current()->uri();
$url = URL::to("/");
$setid = 1;
$setts = DB::table('settings')
        ->where('id', '=', $setid)
        ->get();
?>
@include('style')
@include('header')

   <div class="hdng-cnt">
                    <h2>We are Launching Soon!</h2>
                    <h4>OneStop Platform To Find Professionals For Your Home And Related Services</h4>
                    <div class="col-md-12 form_move" align="center">
                        <div class="col-md-3"></div>
                            <form action="{{ route('search') }}" method="post" enctype="multipart/form-data" id="formID">
                                <div class="col-md-6">
                                    {!! csrf_field() !!}
                                    <div class="col-md-8 paddingoff">

                                        <input type="text" name="search_text" class="searchtext validate[required]" id="search_text" placeholder="What service do you need?">

                                    </div>
                                    <div class="col-md-4 paddingoff"><input type="submit" name="search" class="searchbtn" value="Get Started"></div>


                                </div>
                            </form>
                            <div class="col-md-3"></div>
                        </div>
                    <div class="header_btn">
                        <div id="myCarousel" class="vertical-slider carousel vertical slide col-md-12" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <button type="button" class="btn_fix">Announcing your requests</button>
                                </div>
                                <div class="item">
                                    <button type="button" class="btn_fix">Supporting your communities</button>
                                </div>
                                <div class="item">
                                    <button type="button" class="btn_fix">Keeping money in your pockets</button>    
                                </div>
                            </div>
                        </div>
                        <div class="text-center">    
                            <button type="button" data-toggle="modal" data-target="#subscrib_popup" class="btn_fix head_bt">Subscribe</button>   
                        </div>    
                    </div> 
                </div>

        <script>
            $(document).ready(function () {
                src = "{{ route('searchajax') }}";
                $("#search_text").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: src,
                            dataType: "json",
                            data: {
                                term: request.term
                            },
                            success: function (data) {
                                response(data);

                            }
                        });
                    },
                    minLength: 1,
                });
            });
        </script>
            <script>


                $(document).ready(function () {

                $('.btn-vertical-slider').on('click', function () {

                if ($(this).attr('data-slide') == 'next') {
                $('#myCarousel').carousel('next');
                }
                if ($(this).attr('data-slide') == 'prev') {
                $('#myCarousel').carousel('prev')
                }

                });
                });
                jQuery(".countDown_interval_basic_cont_description").removeClass("countDown_interval_basic_cont_description");
                jQuery(".countDown_interval_cont.countDown_interval_cont_week").before("<div class='countDown_interval_basic_cont_description'>Week<s/div>");
                jQuery(".countDown_interval_cont.countDown_interval_cont_day").before("<div class='countDown_interval_basic_cont_description'>Days</div>");
                jQuery(".countDown_interval_cont.countDown_interval_cont_hour").before("<div class='countDown_interval_basic_cont_description'>Hours</div>");
                jQuery(".countDown_interval_cont.countDown_interval_cont_minute").before("<div class='countDown_interval_basic_cont_description'>Minutes</div>");
                jQuery(".countDown_interval_cont.countDown_interval_cont_second").before("<div class='countDown_interval_basic_cont_description'>Seconds</div>");
                });
                var cd = new Countdown({
                cont: document.querySelector('.check11'),
                        endDate: "June 25 2018 19:00:00", // enter the target date here	
                        outputTranslation: {
                        year: 'Years',
                                week: 'Weeks',
                                day: 'Days',
                                hour: 'Hours',
                                minute: 'Minutes',
                                second: 'Seconds',
                        },
                        endCallback: null,
                        outputFormat: 'week|day|hour|minute|second',
                });
                cd.start();
            </script>


            <section id="three">
                <div class="hdng">
                    <h1 data-wow-delay="0.3s" class="wow bounceInDown" style="visibility: visible; animation-delay: 0.3s; animation-name: bounceInDown;">Our Values And Why Choose Us</h1>
                    <p>We partner with your communities when you choose us for your home and related services</p>
                </div>
                <div class="slider">
                    <div id="myCarousel1" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel1" data-slide-to="1"></li>
                            <li data-target="#myCarousel1" data-slide-to="1"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">

                                <span><b>“</b>The highest of distinctions is service to others.<b>”</b></span>
                                <strong>By King George Vi</strong>
                            </div>
                            <div class="item">
                                <span><b>“</b>No act of kindness, no matter how small, is ever wasted.<b>”</b></span>
                                <strong>By Aesop</strong>
                            </div>
                            <div class="item">
                                <span><b>“</b>You need an attitude of service. You're not just serving yourself. You help others to grow up and you grow with them.<b>”</b></span>
                                <strong>By David Green</strong>
                            </div>
                        </div>
                        <!-- Left and right controls -->
                    </div>
                </div> 
            </section>
            <div class="patner_sec">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4 text-center">
                                <div class="patner_bk_img">
                                    <img src="img/patner_with.png">
                                </div>   
                                <h4>PARTNER WITH US</h4>
                                <p>Partner with us when you use our platform for your services</p>
                            </div>  

                            <div class="col-md-4 text-center"> 
                                <div class="patner_bk_img">
                                    <img src="img/grow_with.png">
                                </div>    
                                <h4>GROW WITH US</h4>
                                <p>Grow your partnership and earn reward points with repeat patronage</p>
                            </div>

                            <div class="col-md-4 text-center">
                                <div class="patner_bk_img">
                                    <img src="img/support_cmmunty.png">
                                </div>   
                                <h4>SUPPORT COMMUNITIES</h4>
                                <p>Support your communities with your reward points</p>
                            </div>   
                        </div>
                    </div>
                </div> 
            </div>


            <section id="one">
                <div class="hdng">
                    <div class="container">
                        <h1 data-wow-delay="0.3s" class="wow bounceInDown" style="visibility: visible; animation-delay: 0.3s; animation-name: bounceInDown;">Our Services</h1>
                        <div class="slider">
                            <div class="col-md-4 col-sm-4">
                                <div class="slider_shdow">
                                    <h2>INDOOR SERVICES</h2>    
                                    <div id="myCarousel11" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel11" data-slide-to="0" class="active"></li>
                                            <li data-target="#myCarousel11" data-slide-to="1"></li>
                                            <li data-target="#myCarousel11" data-slide-to="2"></li>    
                                        </ol>
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <img src="img/inner1.jpeg">
                                            </div>
                                            <div class="item">
                                                <img src="img/inner2.jpg">
                                            </div>
                                            <div class="item">
                                                <img src="img/inner3.jpeg">
                                            </div>

                                        </div>
                                        <!-- Left and right controls -->
                                    </div>     
                                </div>     
                            </div>     
                            <div class="col-md-4 col-sm-4">
                                <div class="slider_shdow">
                                    <h2>OUTDOOR SERVICES</h2>    
                                    <div id="myCarousel10" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel10" data-slide-to="0" class="active"></li>
                                            <li data-target="#myCarousel10" data-slide-to="1"></li>
                                            <li data-target="#myCarousel10" data-slide-to="2"></li>    
                                        </ol>
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <img src="img/outer1.jpeg">
                                            </div>
                                            <div class="item">
                                                <img src="img/outer2.jpeg">
                                            </div>
                                            <div class="item">
                                                <img src="img/outer3.png">
                                            </div>

                                        </div>
                                        <!-- Left and right controls -->
                                    </div>     
                                </div>     
                            </div>     
                            <div class="col-md-4 col-sm-4">
                                <div class="slider_shdow">
                                    <h2>OTHER SERVICES</h2>    
                                    <div id="myCarousel12" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel12" data-slide-to="0" class="active"></li>
                                            <li data-target="#myCarousel12" data-slide-to="1"></li>
                                            <li data-target="#myCarousel12" data-slide-to="2"></li>    
                                        </ol>
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <img src="img/outer_ec2.jpg">
                                            </div>
                                            <div class="item">
                                                <img src="img/outer_ec3.jpg">
                                            </div>
                                            <div class="item">
                                                <img src="img/outer_ec1.jpg">
                                            </div>

                                        </div>
                                        <!-- Left and right controls -->
                                    </div>     
                                </div>     
                            </div>     
                        </div>     

                    </div> 
                </div>
            </section>
            <div class="homtrust_update">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">



                        </div>

                        <section id="how_it_work">
                            <div class="container">
                                <div class="row">
                                    <div class="hdng">
                                        <h1 data-wow-delay="0.3s" class="wow bounceInDown" style="visibility: visible; animation-delay: 0.3s; animation-name: bounceInDown;">How It Works</h1>

                                    </div>
                                    <div class="col-sm-4">
                                        <div class="boder_x">
                                            <img src="{{URL::to('/img/submit_task_icon.png')}}">
                                            <h2>DESCRIBE & SUBMIT YOUR TASK</h2>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="boder_x">
                                            <img src="img/service_provider.png">
                                            <h2>CHOOSE SERVICE PROVIDER</h2>

                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="boder_x">
                                            <img src="img/complete_task_icon.png">
                                            <h2>TASK COMPLETED</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>


                    </div>
                </div>
            </div>
























        @include('footer')
    </body>
</html>