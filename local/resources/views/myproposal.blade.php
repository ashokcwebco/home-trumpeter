 
<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
        <!-- fixed navigation bar -->
        @include('header')
        <div class="heading_login">
            <h1>My Proposals</h1>    
        </div>	
        <div class="login_form">
            <div class="container">
                <div class="dashboard">
                    <div class="col-md-12"> 
                        <div class="cart">
                            <div class="main_table_sec invita_nth">
                                <div class="col-md-12 text-right">
                                    <a href="{{URL::to('/addproposal')}}" class="login_btn servic_bnt" style="background: #f6cd53; float: right;">Add Service</a>
                                </div>
                                <div class="table-responsive table_size_fix">
                                    <div class="scrol_fix_auto">
                                        <table class="table table-borderless">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Services</th>
                                                    <th>Start/End Date</th>
                                                    <th>Created</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                            // echo '<pre>';
                                            // print_r($proposals);die;
                                            ?>
                                            @foreach ($proposals as $key=>$proposal)
                                        <tr>
                                            <td>{{$proposal->title}}</td>
                                            <td>@foreach($proposal->serviceName as $servicename) {{$servicename->subname .','}} @endforeach </td>
                                            <td>{{date('d M',strtotime($proposal->start_date)) .' / '. date('d M',strtotime($proposal->end_date))}}</td>
                                            <td>{{date('Y-m-d',strtotime($proposal->created_at))}}</td>
                                            <td>
                                                <a href="#" data-toggle="tooltip" title="delete" class="red_btn">Remove</a>
                                                <a href="{{URL::to('/invite/'.$proposal->perposal_id)}}" data-toggle="tooltip" title="invite" class="yellow_btn">Invite</a>
                                                <a href="{{URL::to('/viewinvite/'.$proposal->perposal_id)}}" data-toggle="tooltip" class="yellow_btn">View</a>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                              
                                              
                                            </tbody>
                                        </table>
                                    </div>   
                                </div>
                                <!-- <div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate"><a class="paginate_button previous disabled" aria-controls="myTable" data-dt-idx="0" tabindex="0" id="myTable_previous">Previous</a><span><a class="paginate_button current" aria-controls="myTable" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="3" tabindex="0">3</a></span><a class="paginate_button next" aria-controls="myTable" data-dt-idx="7" tabindex="0" id="myTable_next">Next</a><div></div></div> -->
                            </div>       
                        </div>
                    </div>
                </div> 
            </div>
        </div>    
        @include('footer')
    </body>
</html>