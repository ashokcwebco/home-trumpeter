@extends('layouts.app')

@section('content')
@include('style')
@include('header')

<div class="heading_login">
    <h1>Customer/Service Provider Login</h1>    
</div>	

<div class="login_form">
    <div class="container">
        <div class="verticale_align">
            <div class="verticale_middle">
                <div class="main_form">
                    @if(Session::has('success'))
                        <div class="alert alert-success">{{ Session::get('success') }}</div>
                    @endif

                    @if(Session::has('error'))
                        <div class="alert alert-danger">{{ Session::get('error') }}</div>
                     @endif
                    <div class="relativ">    
                        <h6>Sign In</h6>    
                        <img src="images/sign_in form.jpg" alt="" style="display: none;">
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                    <div class="lofin_fomr">    
                        <div class="custom_inp input-effect">

                            <input id="username" type="text" class="effect-16" name="username" value="{{ old('email') }}" required autofocus>
                            <label>Username / E-Mail Address</label>    
                            <span class="focus-border"></span>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="custom_inp input-effect">
                            <input id="password" type="password" class="effect-16" name="password" required>
                            <label>Password</label>
                            <span class="focus-border"></span>
                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="control-group" style="width: 100%">
                            <div class="rempbrt">
                                <input type="checkbox" id="checkbox-1"  class="checkbox-custom" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me

                                       <label for="checkbox-1" class="checkbox-custom-label"> Remember Me </label>
                            </div> 
                            <div class="fotgot">
                                <a href="{{ route('password.request') }}">Forgot Your Password?</a>    
                            </div>    
                        </div>   
                        <button type="submit" class="login_btn">Login</button>    
                    </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>    
@include('footer') 
@endsection
