@extends('layouts.app')

@section('content')
@include('style')
@include('header')


<div class="heading_login">
    <h1>Forgot Your Password?</h1>    
</div>	

<div class="login_form">
    <div class="container">
        <div class="verticale_align">
            <div class="verticale_middle">
                <div class="main_form">
                    <div class="relativ">    
                        <h6>Reset Password</h6>    
                        <img src="img/sign_in form.jpg" alt="" style="display: none;">
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <div class="lofin_fomr">    
                            <div class="custom_inp input-effect">
                            <input id="email" type="email" class="effect-16" name="email" value="{{ old('email') }}" required>
                                <label>E-Mail Address</label>    
                                <span class="focus-border"></span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button type="submit" class="login_btn" style="background: #f6cd53;">Send Password Reset Link</button>    
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  

@include('footer')
@endsection
