@extends('layouts.app')

@section('content')
@include('style')
@include('header')


 <div class="heading_login">
                <h1>Customer/Service Provider Sign up</h1>    
            </div>	

            <div class="login_form">
                <div class="container">
                    <div class="verticale_align">
                        <div class="verticale_middle">
                            <div class="main_form">
                                <div class="relativ">
                                    <ul class="nav nav-tabs service_tab">
                                        <li class="{{isset($userData->admin) && $userData->admin ==0? '':'active'}}">
                                        <a data-toggle="tab" id="home-tab"  href="#home">Service Provider</a></li>
                                        <li class="{{isset($userData->admin) && $userData->admin ==0? 'active':''}}"><a data-toggle="tab" id="document-tab" href="#menu1">Customer</a></li>
                                    </ul>
                                </div>
                                <div class="lofin_fomr">  
                                    <div class="tab-content">    
                                        <div id="home" class="tab-pane fade {{isset($userData->admin) && $userData->admin ==0? '':'active in'}}">    
                                            @if(Session::has('success'))
                                                <div class="alert alert-success">
                                                    {{ Session::get('success') }}
                                                </div>
                                            @endif
                                            @if(Session::has('error'))
                                                <div class="alert alert-danger">
                                                    {{ Session::get('error') }}
                                                </div>
                                            @endif
                                           
                                            <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                                                {{ csrf_field() }}
                                                <div class="custom_inp input-effect">
                                                    <input id="name" type="text" class="effect-16" name="name" value="<?php echo isset($userData->name)?$userData->name:'';?>" required autocomplete="off">
                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                    <label>Username</label>    
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <input id="name" type="text" class="effect-16" name="fname" value="<?php echo isset($userData->fname)?$userData->fname:'';?>" required autocomplete="off">
                                                    @if ($errors->has('fname'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('fname') }}</strong>
                                                        </span>
                                                    @endif
                                                    <label>First Name</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <input id="name" type="text" class="effect-16" name="lname" value="<?php echo isset($userData->lname)?$userData->lname:'';?>" required autocomplete="off">
                                                    @if ($errors->has('lname'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('lname') }}</strong>
                                                        </span>
                                                    @endif
                                                    <label>Last Name</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <input id="email" type="email" class="effect-16" name="email" value="<?php echo isset($userData->name)?$userData->email:'';?>" required autocomplete="off">
                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                    <label>E-Mail Address</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <input id="password" type="password" class="effect-16" name="password" <?php echo !isset($user_id)? "required":'';?> autocomplete="off">
                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                    <label>Password</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                @if(!isset($user_id))
                                                <div class="custom_inp input-effect">
                                                    <input id="password-confirm" type="password" class="effect-16" name="password_confirmation" required autocomplete="off">
                                                    <label>Confirm Password</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                @endif
                                                <div class="custom_inp input-effect">
                                                    <input id="phoneno" type="text" class="effect-16" name="phoneno"  value="<?php echo isset($userData->phone)?$userData->phone:'';?>" required autocomplete="off">
                                                    <label>Phone No</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <div class="select">
                                                        <select name="service_type" required>
                                                            <option value="">Service Type</option>
                                                            <option value="1" {{isset($userData->service_type) && $userData->service_type == 1?'selected':''}}>Individual</option>
                                                            <option value="2" {{isset($userData->service_type) && $userData->service_type == 2?'selected':''}}>Business</option>
                                                            <option value="3" {{ isset($userData->service_type) && $userData->service_type == 3?'selected':''}}>Both</option>
                                                        </select>
                                                    </div>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <div class="select">
                                                        <select name="gender" required>
                                                            <option value="">Gender</option>
                                                            <option value="male" {{isset($userData->gender) && $userData->gender == 'male'?'selected="selected"':''}}>Male</option>
                                                            <option value="female" {{isset($userData->gender) && $userData->gender == 'female'?'selected="selected"':''}}>Female</option>
                                                        </select>
                                                    </div>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <input type="hidden" name="savepassword" value="<?php echo isset($userData->password)?$userData->password:'';?>">
                                                <input type="hidden" name="usertype" value="2"/>
                                                <input type="hidden" name="id" value="<?php echo isset($userData->id)? $userData->id:'';?>">
                                                <input type="submit" class="login_btn" value="Save and Continue"> 

                                            </form> 
                                            @if(isset($user_id) && $user_id)
                                                <form class="form-horizontal" role="form" method="POST" action="{{ route('uploaddocs') }}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                    <div class="custom_inp input-effect">
                                                        <input type="file" id="trade" name="trade" class="effect-16">
                                                        @if ($errors->has('trade'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('trade') }}</strong>
                                                            </span>
                                                        @endif
                                                        <label>Certificate of Trade</label>    
                                                        <span class="focus-border"></span>
                                                    </div>
                                                    <div class="custom_inp input-effect">
                                                    <input type="file" id="proofInsurance" name="proofInsurance"  class="effect-16">
                                                        @if ($errors->has('proofInsurance'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('proofInsurance') }}</strong>
                                                            </span>
                                                        @endif
                                                        <label>Proof of insurance</label>    
                                                        <span class="focus-border"></span>
                                                    </div>
                                                    <div class="custom_inp input-effect">
                                                    <input type="file" id="profilePhoto" name="photo" class="effect-16">
                                                        @if ($errors->has('photo'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('photo') }}</strong>
                                                            </span>
                                                        @endif
                                                        <label>Profile Photo</label>    
                                                        <span class="focus-border"></span>
                                                    </div>
                                                    <input type="submit" class="login_btn" value="Submit"> 
                                                </form>
                                            @endif
                                        </div> 
                                        <div id="menu1" class="tab-pane fade {{ isset($userData->admin) && $userData->admin ==0? 'active in':''}}">
                                            <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                                                {{ csrf_field() }}
                                                <div class="custom_inp input-effect">
                                                    <input id="name" type="text" class="effect-16" name="name" value="<?php echo isset($userData->name)?$userData->name:'';?>" required autocomplete="off">
                                                    <label>Username</label>    
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <input id="name" type="text" class="effect-16" name="fname" value="<?php echo isset($userData->fname)?$userData->fname:'';?>" required autocomplete="off">
                                                    <label>First Name</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <input id="name" type="text" class="effect-16" name="lname" value="<?php echo isset($userData->lname)?$userData->lname:'';?>" required autocomplete="off">
                                                    <label>Last Name</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <input id="email" type="email" class="effect-16" name="email" value="<?php echo isset($userData->email)?$userData->email:'';?>" required autocomplete="off">
                                                    <label>E-Mail Address</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                    <input id="password" type="password" class="effect-16" name="password" required>
                                                    <label>Password</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                @if(!isset($userData->id))
                                                <div class="custom_inp input-effect">
                                                    <input id="password-confirm" type="password" class="effect-16" name="password_confirmation" required>
                                                    <label>Confirm Password</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                @endif
                                                <div class="custom_inp input-effect">
                                                    <input id="phoneno" type="text" class="effect-16" name="phoneno" value="<?php echo isset($userData->phone)?$userData->phone:'';?>" required>
                                                    <label>Phone No</label>
                                                    <span class="focus-border"></span>
                                                </div>
                                                <div class="custom_inp input-effect">
                                                <div class="select"> 
                                                    <select name="gender" required>
                                                    <option value="">Gender</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                    </select>
                                                    </div>
                                                    
                                                    <span class="focus-border"></span>
                                                </div>
                                                <input type="hidden" name="usertype" value="0"/>
                                                <input type="submit" class="login_btn" value="Register">
                                            </form>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    





            <footer>
                <div class="footer text-center footer2">
                    © Copyright 2018 Hometrumpeter
                </div>
            </footer>    

        </div>  
@endsection
