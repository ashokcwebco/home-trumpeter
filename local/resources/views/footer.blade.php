<?php

use Illuminate\Support\Facades\Route;

$ncurrentPath = Route::getFacadeRoot()->current()->uri();
$url = URL::to("/");
$setid = 1;
$setts = DB::table('settings')
        ->where('id', '=', $setid)
        ->get();
?>
  <footer>
                <div class="footer text-center footer2">
                    © Copyright 2018 Hometrumpeter
                </div>
            </footer> 
</div>

<!-- add javascripts -->

<?php if ($ncurrentPath == "index" or $ncurrentPath == "/") { ?>
    <script>


        // $(document).ready(function () {
        //     $(window).scroll(function () {
        //         if ($(window).width() > 1200)
        //         {
        //             // check if scroll event happened
        //             if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
        //                 $(".navbar-fixed-top").css("background-color", "#F4F4F4");

        //                 $(".navbar-fixed-top li a").css("color", "#000000");


        //                 $(".navbar-fixed-top li.dropdown .open a").css("color", "#000000");
        //                 // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
        //             } else {
        //                 $(".navbar-fixed-top").css("background-color", "transparent");

        //                 $(".navbar-fixed-top li a").css("color", "#ffffff");
        //                 $(".navbar-fixed-top li.dropdown .open a").css("color", "#000000");
        //                 // if not, change it back to transparent
        //             }



        //         }

        //     });
        // });
    </script>
<?php } else { ?>
    <script>
        // function to set the height on fly
     
        function autoHeight() {
                $('.login_form').css('min-height', 0);
                $('.login_form').css('min-height', (
                    parseInt($(document).height())
                    - parseInt($('header').height())
                    - parseInt($('footer').height())
                    - parseInt($('footer').height())
                    - parseInt(135)
                ));
                }

 // onDocumentReady function bind
 $(document).ready(function() {
   autoHeight();
 });

 // onResize bind of the function
 $(window).resize(function() {
   autoHeight();
 });

       

        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(window).width() > 1200)
                {
                    // check if scroll event happened
                    if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
                        $(".navbar-fixed-top").css("background-color", "#F4F4F4");
                        $(".navbar-fixed-top li a").css("color", "#000000");
                        // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
                    } else {
                        $(".navbar-fixed-top").css("background-color", "#ffffff");
                        $(".navbar-fixed-top li a").css("color", "#000000");
                        // if not, change it back to transparent
                    }
                }

            });
        });
    </script>
<?php } ?>
<!-- Only used for Script Tutorial's Demo site. Please ignore and remove. -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#media').carousel({
            pause: true,
            interval: false,
        });
    });




</script>

<script type="text/javascript" src="js/wow.js"></script>
<script type="text/javascript" src="js/script.js"></script>

<script type="text/javascript" src="js/jquery.flexisel.js"></script>


<script src="<?php echo $url; ?>/js/jquery.multiselect.js"></script>
<script>
    $('#langOpt').multiselect({
        columns: 1,
        placeholder: 'Select Services'
    });
</script>
<script>
    $(document).ready(function () {
        $(".toggle_menu").click(function () {
            $(".sidenav").addClass("back_layer");
        });
    });
    function openNav() {
        document.getElementById("mySidenav").style.right = "0px";
    }

    function closeNav() {
        $(".sidenav").removeClass("back_layer");
        document.getElementById("mySidenav").style.right = "-290px";
    }
</script>
{!!Html::script('local/resources/assets/js/perposal.js')!!}
<?php
/* ?><script src="{{ asset('js/app.js') }}"></script><?php */?>