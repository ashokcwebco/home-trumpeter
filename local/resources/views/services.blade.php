 
<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
        <!-- fixed navigation bar -->
        @include('header')
        <!-- slider -->
        <div class="heading_login">
            <h1>Services</h1>    
        </div>	
        <div class="login_form">
            <div class="container">
                <div class="dashboard">
                    <div class="col-md-12"> 
                        <div class="cart">
                            <div class="service_form">
                                @if(Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                                @endif
                                @if(Session::has('error'))
                                <div class="alert alert-danger">
                                    {{ Session::get('error') }}
                                </div>
                                @endif
                                <form class="form-horizontal" role="form" method="POST" action="{{ route('services') }}" id="formID" enctype="multipart/form-data">
                                    {!! csrf_field() !!}
                                    <div class="col-md-3">
                                        <div class="control-group">   
                                            <input type="hidden" name="editid" value="{{$editid}}"> 
                                            <div class="select">
                                                <select class="validate[required]" id="subservice_id" name="subservice_id" required>
                                                    <option value="">Select Services</option>
                                                    <?php foreach ($services as $disp) { ?>
                                                        <option value="<?php echo $disp->id; ?>" disabled><?php echo $disp->name; ?></option>
                                                        <?php
                                                        $subservices = DB::table('subservices')->where('service', '=', $disp->id)->orderBy('subname', 'asc')->get();
                                                        foreach ($subservices as $dispsub) {
                                                            ?>
                                                            <option value="<?php echo $dispsub->subid; ?>" <?php
                                                            if (!empty($sellservices)) {
                                                                if ($sellservices->subservice_id == $dispsub->subid) {
                                                                    ?> selected <?php
                                                                        }
                                                                    }
                                                                    ?>> -- <?php echo $dispsub->subname; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                </select>
                                                <div class="select__arrow"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">    
                                            <div class="select">
                                                <select class="validate[required]" id="serviceType" name="serviceType" onChange="checkCondition(this)" required>
                                                    <option value="">Select Service Type</option>
                                                    @foreach ($serviceTypes as $serviceType)
                                                    <option value="{{$serviceType->s_type_id}}" {{ isset($sellservices->service_type) && $sellservices->service_type == $serviceType->s_type_id?'selected':''}}>{{$serviceType->service_type}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="select__arrow"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" id="condition-sec"  style="display:{{isset($sellservices->service_type) && $sellservices->service_type ==3? 'block':'none'}}">
                                        <div class="control-group">    
                                            <div class="select">
                                                <select class="validate[required]" id="condition" name="condition"  {{isset($sellservices->service_type) && $sellservices->service_type ==3? 'required':''}}>
                                                    <option value="">Conditions</option>
                                                        @foreach ($conditions as $condition)
                                                            <option value="{{$condition->condition_id}}"  {{ isset($sellservices->conditions) && $sellservices->conditions == $condition->condition_id?'selected':''}}>{{$condition->title}}</option>
                                                        @endforeach
                                                </select>
                                                <div class="select__arrow"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="custom_inp input-effect">
                                            <input type="text"  name="" id="" class="effect-16 validate[required] text-input" disabled="disabled" value="<?php echo $setting[0]->site_currency; ?>">
                                            <label>Currency</label>    
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom_inp input-effect">
                                        <input type="text"  name="price" required id="price" class="effect-16 validate[required] text-input" value="{{(!empty($sellservices))? $sellservices->price:''}}">
                                            <label>Price *</label>    
                                            <span class="focus-border"></span>
                                        </div></div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3"><div class="custom_inp input-effect">
                                         <input type="text" name="time" id="time" class="effect-16 validate[required] text-input" value="{{ (!empty($sellservices))?$sellservices->time:''}}">
                                         <label>Time (Hours)</label>    
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                        <input type="hidden" name="user_id" value="{{$uuid}}">
                                        <input type="hidden" name="shop_id" value="{{$shopview[0]->id}}">
                                    <div class="col-md-9">
                                        <a href="{{URL::to('/services')}}" class="login_btn servic_bnt" style="background: #f6cd53;">Reset</a>
                                        <input type="submit" class="login_btn servic_bnt" style="background: #f6cd53;" value="Submit">
                                    </div>
                                </form>                               
                            </div>
                            <div class="main_table_sec service_nth">
                                <div class="col-md-12 text-right">
                                    <a href="{{URL::to('/services')}}"  class="login_btn servic_bnt" style="background: #f6cd53; float: right;">Add Service</a>
                                </div>
                                <div class="table-responsive table_size_fix">
                                    <div class="scrol_fix_auto">
                                        <table class="table table-borderless">
                                            <thead>
                                                <tr>
                                                     <th>Sno</th>
                                                    <th>Services</th>
                                                    <th>Price</th>
                                                    <th>Time</th>
                                                    <th>Update</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($viewservice as $key=>$newserve)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{$newserve->subname}}</td>
                                                    <td>{{$newserve->price . ' ' . $setting[0]->site_currency}}</td>
                                                    <td>{{$newserve->time}}</td>
                                                    <td>
                                                        <a href="{{URL::to('/services/'.$newserve->id)}}" class="yellow_btn">Edit</a>
                                                    </td>
                                                    <td>
                                                        <a href="{{URL::to('/services/'.$newserve->id.'/delete')}}" class="red_btn" onclick="return confirm('Are you sure you want to delete this?')">Remove</a>
                                                    </td>
                                                </tr>
                                               @endforeach
                                            </tbody>
                                        </table>
                                    </div>   
                                </div>
                                <!-- <div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate"><a class="paginate_button previous disabled" aria-controls="myTable" data-dt-idx="0" tabindex="0" id="myTable_previous">Previous</a><span><a class="paginate_button current" aria-controls="myTable" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="3" tabindex="0">3</a></span><a class="paginate_button next" aria-controls="myTable" data-dt-idx="7" tabindex="0" id="myTable_next">Next</a><div></div></div> -->
                            </div>       
                        </div>
                    </div>
                </div> 
            </div>
        </div>    

        @include('footer')
    </body>
</html>
<script>
    function checkCondition(e) {
        if (e.value == 3) {
            document.getElementById('condition-sec').style.display = "block";
            document.getElementById("condition").required = true;
        } else {
            document.getElementById('condition-sec').style.display = "none";
            document.getElementById("condition").required = false;
            document.getElementById('condition').value = "";
        }

    }
</script>