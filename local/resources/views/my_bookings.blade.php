<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
        <?php $url = URL::to("/"); ?>
        <!-- fixed navigation bar -->
        @include('header')
        <!-- slider -->
        <div class="clearfix"></div>
        <div class="video">
            <div class="clearfix"></div>
            <div class="headerbg">
                <div class="col-md-12" align="center"><h1>My Bookings</h1></div>
            </div>
            <div class="container">
                <div class="height30"></div>
                <div class="row">
                    <?php
                    if ($count == 0) {
                        ?>
                        <div class="err-msg" align="center">No booking found!</div>
                        <?php
                    } else {
                        $iq = 1;
                        foreach ($booking as $book) {
                            $ser_id = $book->services_id;
                            $sel = explode(",", $ser_id);
                            $lev = count($sel);
                            $ser_name = "";
                            $sum = "";
                            $price = "";
                            for ($i = 0; $i < $lev; $i++) {
                                $id = $sel[$i];
                                $fet1 = DB::table('subservices')
                                        ->where('subid', '=', $id)
                                        ->get();
                                $ser_name.='<div class="book-profile radiusoff">' . $fet1[0]->subname . '</div>';
                                $ser_name.=",";
                                $ser_name = trim($ser_name, ",");
                            }
                            $shop_id    = $book->shop_id;
                            $buser = $book->user_id;
                            $bookk = $book->book_id;
                            $booking_time = $book->booking_time;
                            if ($booking_time > 12) {
                                $final_time = $booking_time - 12;
                                $final_time = $final_time . "PM";
                            } else {
                                $final_time = $booking_time . "AM";
                            }
                            $viewrating = DB::table('rating')
                                    ->where('rbook_id', $bookk)
                                    ->where('user_id', $user_id)
                                    ->get();
                            $nucount = DB::table('rating')
                            ->where('rbook_id', $bookk)
                            ->where('user_id', $user_id)
                                    ->count();
                            $checkcounte = DB::table('booking')
                                    ->where('shop_id', '=', $shop_id)
                                    ->where('user_id', '=', $buser)
                                    ->count();
                            ?>
                            <script>
                                (function(e){var t, o = {className:"autosizejs", append:"", callback:!1, resizeDelay:10}, i = '<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>', n = ["fontFamily", "fontSize", "fontWeight", "fontStyle", "letterSpacing", "textTransform", "wordSpacing", "textIndent"], s = e(i).data("autosize", !0)[0]; s.style.lineHeight = "99px", "99px" === e(s).css("lineHeight") && n.push("lineHeight"), s.style.lineHeight = "", e.fn.autosize = function(i){return this.length?(i = e.extend({}, o, i || {}), s.parentNode !== document.body && e(document.body).append(s), this.each(function(){function o(){var t, o; "getComputedStyle"in window?(t = window.getComputedStyle(u, null), o = u.getBoundingClientRect().width, e.each(["paddingLeft", "paddingRight", "borderLeftWidth", "borderRightWidth"], function(e, i){o -= parseInt(t[i], 10)}), s.style.width = o + "px"):s.style.width = Math.max(p.width(), 0) + "px"}function a(){var a = {}; if (t = u, s.className = i.className, d = parseInt(p.css("maxHeight"), 10), e.each(n, function(e, t){a[t] = p.css(t)}), e(s).css(a), o(), window.chrome){var r = u.style.width; u.style.width = "0px", u.offsetWidth, u.style.width = r}}function r(){var e, n; t !== u?a():o(), s.value = u.value + i.append, s.style.overflowY = u.style.overflowY, n = parseInt(u.style.height, 10), s.scrollTop = 0, s.scrollTop = 9e4, e = s.scrollTop, d && e > d?(u.style.overflowY = "scroll", e = d):(u.style.overflowY = "hidden", c > e && (e = c)), e += w, n !== e && (u.style.height = e + "px", f && i.callback.call(u, u))}function l(){clearTimeout(h), h = setTimeout(function(){var e = p.width(); e !== g && (g = e, r())}, parseInt(i.resizeDelay, 10))}var d, c, h, u = this, p = e(u), w = 0, f = e.isFunction(i.callback), z = {height:u.style.height, overflow:u.style.overflow, overflowY:u.style.overflowY, wordWrap:u.style.wordWrap, resize:u.style.resize}, g = p.width(); p.data("autosize") || (p.data("autosize", !0), ("border-box" === p.css("box-sizing") || "border-box" === p.css("-moz-box-sizing") || "border-box" === p.css("-webkit-box-sizing")) && (w = p.outerHeight() - p.height()), c = Math.max(parseInt(p.css("minHeight"), 10) - w || 0, p.height()), p.css({overflow:"hidden", overflowY:"hidden", wordWrap:"break-word", resize:"none" === p.css("resize") || "vertical" === p.css("resize")?"none":"horizontal"}), "onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize", r):p.on("propertychange.autosize", function(){"value" === event.propertyName && r()}):p.on("input.autosize", r), i.resizeDelay !== !1 && e(window).on("resize.autosize", l), p.on("autosize.resize", r), p.on("autosize.resizeIncludeStyle", function(){t = null, r()}), p.on("autosize.destroy", function(){t = null, clearTimeout(h), e(window).off("resize", l), p.off("autosize").off(".autosize").css(z).removeData("autosize")}), r())})):this}})(window.jQuery || window.$);
                                var __slice = [].slice; (function(e, t){var n; n = function(){function t(t, n){var r, i, s, o = this; this.options = e.extend({}, this.defaults, n); this.$el = t; s = this.defaults; for (r in s){i = s[r]; if (this.$el.data(r) != null){this.options[r] = this.$el.data(r)}}this.createStars(); this.syncRating(); this.$el.on("mouseover.starrr", "span", function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget) + 1)}); this.$el.on("mouseout.starrr", function(){return o.syncRating()}); this.$el.on("click.starrr", "span", function(e){return o.setRating(o.$el.find("span").index(e.currentTarget) + 1)}); this.$el.on("starrr:change", this.options.change)}t.prototype.defaults = {rating:void 0, numStars:5, change:function(e, t){}}; t.prototype.createStars = function(){var e, t, n; n = []; for (e = 1, t = this.options.numStars; 1 <= t?e <= t:e >= t; 1 <= t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n}; t.prototype.setRating = function(e){if (this.options.rating === e){e = void 0}this.options.rating = e; this.syncRating(); return this.$el.trigger("starrr:change", e)}; t.prototype.syncRating = function(e){var t, n, r, i; e || (e = this.options.rating); if (e){for (t = n = 0, i = e - 1; 0 <= i?n <= i:n >= i; t = 0 <= i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if (e && e < 5){for (t = r = e; e <= 4?r <= 4:r >= 4; t = e <= 4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if (!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}; return t}(); return e.fn.extend({starrr:function(){var t, r; r = arguments[0], t = 2 <= arguments.length?__slice.call(arguments, 1):[]; return this.each(function(){var i; i = e(this).data("star-rating"); if (!i){e(this).data("star-rating", i = new n(e(this), r))}if (typeof r === "string"){return i[r].apply(i, t)}})}})})(window.jQuery, window); $(function(){return $(".starrr").starrr()})
                                        $(function(){
                                        var ratingsField = $('#ratings-hidden{{$book->book_id}}');
                                        $('.starrr').on('starrr:change', function(e, value){
                                        ratingsField.val(value);
                                        });
                                        });
                            </script>
                            <div class="row booking_page">
                                <div class="shop_pic col-lg-4">
                                    <?php
                                    $shopphoto = "/shop/";
                                    $paths = '/local/images' . $shopphoto . $book->cover_photo;
                                    if ($book->cover_photo != "") {
                                        ?>
                                        <a href="<?php echo $url; ?>/vendor/<?php echo $book->name; ?>" target="_blank"><img class="img-responsive newcsl" src="<?php echo $url . $paths; ?>" alt=""></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $url; ?>/vendor/<?php echo $book->name; ?>" target="_blank">	<img class="img-responsive" src="<?php echo $url . '/local/images/noimage.jpg'; ?>" alt="" style="min-width:400px; max-height:200px;"></a>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-4 paddingleft20">
                                    <h3 class="sv_shop_style"><a href="<?php echo $url; ?>/vendor/<?php echo $book->name; ?>" target="_blank"><?php echo $book->shop_name; ?></a></h3>
                                    <p><span class="lnr lnr-calendar-full"></span>	<?php echo $book->booking_date; ?> - <span class="lnr lnr-clock"></span>
                                        <?php echo $final_time; ?></p>
                                    <h5>Booking Id : <?php echo $book->book_id; ?></h5>
                                    <?php echo $ser_name; ?>
                                    <div class="total-price radiusoff">Total Price - <?php
                                        if ($book->total_amt == "") {
                                            echo "0";
                                        } else {
                                            echo $book->total_amt;
                                        }
                                        ?>&nbsp;<?php echo $book->currency; ?></div>
                                </div>
                                <div class="col-lg-4 book_content">
                                    <div class="col-md-12 paddingtop20">
                                        <div class="well-sm mtop10">
                                            <div class="revbtn">
												@if($book->is_completed == 1)
                                                <span style="color:#0DE50D; float: right;">Completed</span>
												@elseif($book->is_completed ==2)
												<div class="btn-sect">
													<a class="btn btn-success btn-green radiusoff booking-complete" rel="{{$book->book_id}}" href="javascript:;" >complete</a>
													<a class="btn btn-success btn-green radiusoff booking-dispute" rel="{{$book->book_id}}" href="javascript:;" >dispute</a>
                                                </div>
                                                @else
                                                <div class="btn-sect">
													<a class="btn btn-success btn-green radiusoff booking-complete" rel="{{$book->book_id}}" href="javascript:;" >complete</a>
													<a class="btn btn-success btn-green radiusoff booking-dispute" rel="{{$book->book_id}}" href="javascript:;" >dispute</a>
                                                </div>
												@endif

                                            </div>
                                            

                                             <div class="row" id="post-dispute<?php echo $bookk; ?>"  style="display:none;">
                                                <div class="col-md-12">
                                                    
                                                    <form accept-charset="UTF-8" action="{{ route('dispute') }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" id="book_id" name="book_id" value="{{$book->book_id}}">
                                                        <textarea required class="form-control animated radiusoff" cols="50"  name="message" placeholder="Enter your message here..." rows="5"></textarea>
                                                        <div class="text-right">
                                                                <button class="btn btn-success btn-md radiusoff" type="submit">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="row" id="post-review-box<?php echo $bookk; ?>"  style="display: {{(!isset($viewrating[0]->rid) && $book->is_completed == 1)?'block':'none'}}">
                                                <div class="col-md-12">
                                                    
                                                    <form accept-charset="UTF-8" action="{{ route('my_bookings') }}" method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="rate_id" value="<?php
                                                        if (!empty($nucount)) {
                                                            echo $viewrating[0]->rid;
                                                        }
                                                        ?>">
                                                        <input id="" name="shop_id" type="hidden" value="{{$shop_id}}"> 
                                                        <input id="ratings-hidden{{$book->book_id}}" name="rating" type="hidden"> 
                                                        <input type="hidden" id="book_id" name="book_id" value="{{$book->book_id}}">
                                                        <textarea required class="form-control animated radiusoff" cols="50"  name="comment" placeholder="Enter your review here..." rows="5"><?php
                                                            if (!empty($nucount)) {
                                                                echo $viewrating[0]->comment;
                                                            }
                                                            ?></textarea>
                                                        <div class="text-right">
                                                            <div class="stars starrr" data-rating="<?php
                                                            if (!empty($nucount)) {
                                                                echo $viewrating[0]->rating;
                                                            }
                                                            ?>"></div>
                                                            <a class="btn btn-danger btn-md radiusoff" href="#"  style="display:none; margin-right: 10px;">
                                                                Cancel</a>
                                                                <button class="btn btn-success btn-md radiusoff" type="submit">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="row" id="posted-review<?php echo $bookk; ?>"  style="display:{{isset($viewrating[0]->rid)?'':'none'}}">
                                                <div class="col-md-12">
                                                    <div class="animated radiusoff">{{isset($viewrating[0]->comment)? $viewrating[0]->comment:''}}</div>
                                                        <div class="stars starrr" data-rating="{{isset($viewrating[0]->rating)? $viewrating[0]->rating:''}}"></div>
                                                   
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>		
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        @include('footer')
        <?php if (session()->has('message')) { ?>
            <script type="text/javascript">
                alert("<?php echo session()->get('message'); ?>");
            </script>
        </div>
	<?php } ?>
	
	<script>

		jQuery('.booking-complete').on('click',function(){
			var butn = $(this).parent('div');
			var book_id   = $(this).attr('rel');
			var data = JSON.stringify({"book_id": $(this).attr('rel')});
			src = "{{URL::to('/complete-booking/') }}";
			var xhr = new XMLHttpRequest();
			xhr.open("GET", src+'/'+book_id, true);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.onreadystatechange = function () {
				if (xhr.readyState === 4 && xhr.status === 200) {
					butn.fadeOut(100);
					var closeReviewBtn = $('#close-review-box'+book_id);
                    var ratingsField = $('#ratings-hidden'+book_id);
					var reviewBox = $('#post-review-box'+book_id);
					reviewBox.slideDown(400);
					$('.starrr').on('starrr:change', function(e, value){
						ratingsField.val(value);
					});
				}
			};
			xhr.send();

		});


        jQuery('.booking-dispute').on('click',function(){
            var reviewBox = $('#post-dispute'+$(this).attr('rel'));
            reviewBox.slideDown(400);
			// var butn = $(this).parent('div');
			// var book_id   = $(this).attr('rel');
			// var data = JSON.stringify({"book_id": $(this).attr('rel')});
			// src = "{{URL::to('/complete-booking/') }}";
			// var xhr = new XMLHttpRequest();
			// xhr.open("GET", src+'/'+book_id, true);
			// xhr.setRequestHeader("Content-Type", "application/json");
			// xhr.onreadystatechange = function () {
			// 	if (xhr.readyState === 4 && xhr.status === 200) {
			// 		butn.fadeOut(100);
			// 		var closeReviewBtn = $('#close-review-box'+book_id);
            //         var ratingsField = $('#ratings-hidden'+book_id);
			// 		var reviewBox = $('#post-review-box'+book_id);
			// 		reviewBox.slideDown(400);
			// 		$('.starrr').on('starrr:change', function(e, value){
			// 			ratingsField.val(value);
			// 		});
			// 	}
			// };
			// xhr.send();

		});
	
	</script>
</body>
</html>