<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
    @include('header')
        <meta name="_token" content="{{csrf_token()}}" />
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div class="headerbg">
                <div class="col-md-12" align="center"><h1>Chat</h1></div>
            </div>
        <div class="container">
            <div class="messaging">
                <div class="inbox_msg">
                    <div class="inbox_people">
                        <div class="headind_srch">
                            <div class="recent_heading">
                                <h4>{{$invitation->title}}</h4>
                            </div>
                        </div>
                        <div class="inbox_chat">
                            <div class="chat_list active_chat">
                                <div class="chat_people">
                                    <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                    <div class="chat_ib">
                                        <h5>{{$invitation->senderfname.' '.$invitation->senderlname}}<span class="chat_date">{{date('d M Y', strtotime($invitation->created_at))}}</span></h5>
                                        <p>{{$invitation->description}}</p>
                                    </div>
                                </div>
                            </div>
                   
                        </div>
                    </div>
                    <div class="mesgs">
                        <div class="msg_history">
                        </div>
                        <div class="type_msg">
                            <div class="input_msg_write">
                            <form id="file-form" action="" method="POST">
                                <label><i class="fa fa-paperclip" ></i>
                                    <input type="file"  rel="{{$invitation->invite_id}}" id="file-select" name="photos[]" multiple/>    
                                </label>
                            </form> 
                                <input type="text" class="write_msg" placeholder="Type a message" />
                                <!-- <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('footer')
        <script>
            var inviteId = "{{$invitation->invite_id}}";
             loaddefault(inviteId);
        $(document).on('change','#file-select',function(e){
            var fileSelect = document.getElementById('file-select');
            var files = fileSelect.files;
            var formData = new FormData();
            formData.append('inviteId', fileSelect.getAttribute('rel'));
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                formData.append('photos', file, file.name);
                }
                src = "{{ route('sendfile') }}";
            
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            console.log(percentComplete);
                        }
                }, false);

                xhr.open('POST', src, true);
                xhr.setRequestHeader('X-CSRF-Token', $('input[name="_token"]').val());
                xhr.onload = function () {
                    if (xhr.status === 200) {
                        res = JSON.parse(xhr.response);
                        if(res.data.length>0){
                            for(var i=0; i< res.data.length; i++ ){
                            var whoisUser =  (res.data[i].user_Id == AuthUser)?'me':'service';
                                insertChat(whoisUser, res.data[i].message,0,res.data[i].invite_id);
                            }
                        } 
                    } else {
                    }
                };
                xhr.send(formData);
        
        });

        setInterval(function(){ loadDataIntervel() }, 3000);

        function loadDataIntervel(){
            src = "{{ route('loaddataintervel') }}";
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                $.ajax({
                    type: "POST",
                    url:src,
                    data: {inviteId:inviteId},
                    success: function( res ) {
                        res = JSON.parse(res);
                        if(res.data.length>0){
                            for(var i=0; i< res.data.length; i++ ){
                            var whoisUser =  (res.data[i].user_Id == AuthUser)?"me":"service";
                                insertChat(whoisUser, res.data[i].message,0,res.data[i].invite_id);
                            }
                        }  
                    }
                });
        }



var AuthUser = '<?php echo Auth::user()->id; ?>';
var me = {};
var you = {};

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}            

//-- No use time. It is a javaScript effect.
function insertChat(who, text, time = 0){
    var control = "";
    var date = formatAMPM(new Date());
    
    if (who == "me"){
        
        control =   '<div class="outgoing_msg">' +
                        '<div class="sent_msg">' +
                            '<p>'+text+'</p>'+
                            '<span class="time_date">'+date+'</span></div>'+
                    '</div>';                  
    }else{
        control =   '<div class="incoming_msg">'+
                    '<div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>'+
                    '<div class="received_msg">'+
                            '<div class="received_withd_msg">'+
                                '<p>'+text+'</p>'+
                                '<span class="time_date">'+date+'</span></div>'+
                        '</div>'+
                    '</div>';
                
    }
    setTimeout(
        function(){                        
            $(".msg_history").append(control);

        }, time);
    
}
function save(msg){
        src = "{{ route('pushmessage') }}";
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
        $.ajax({
            type: "POST",
            url:src,
            data: {msg:msg,inviteId:inviteId},
            success: function( res ) {
                res = JSON.parse(res);
                if(res.data.length>0){
                    for(var i=0; i< res.data.length; i++ ){
                       var whoisUser =  (res.data[i].user_Id == AuthUser)?'me':'service';
                        insertChat(whoisUser, res.data[i].message,0);
                    }
                }  
            }
        });
}

$(document).on("keyup",".write_msg", function(e){

    if (e.which == 13){
        var text = $(this).val();
        if (text !== ""){
            save(text);             
            $(this).val('');
        }
    }
});


function loaddefault(inviteId){
        src = "{{ route('getmsgs') }}";
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
        $.ajax({
            type: "POST",
            url:src,
            data: {inviteId:inviteId},
            success: function( res ) {
                res = JSON.parse(res);
                if(res.data.length>0){
                    for(var i=0; i< res.data.length; i++ ){
                       var whoisUser =  (res.data[i].user_Id == AuthUser)?"me":"service";
                        insertChat(whoisUser, res.data[i].message,0);
                    }
                }  
            }
        });
}
        </script>
    </body>
</html>