 
<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
        <?php $url = URL::to("/"); ?>
        <!-- fixed navigation bar -->
        @include('header')
        <meta name="_token" content="{{csrf_token()}}" />
        <!-- slider -->
        <div class="clearfix"></div>
        <div class="video">
            <div class="clearfix"></div>
          
            <div class="container">
<link href="{{URL::to('/css/stylish-portfolio.css')}}" rel="stylesheet">

<form action="{{route('acceptproposal')}}" method="post" >
                {{ csrf_field() }}
                    <input type="hidden" id="invitId" name="inviteId" value="{{$invitation->invite_id}}">
                    <div class="modal-body">
                    <div class="approval-res">
                    <div class="top-sec container-fluid">
                            <div class="row">
                                <div class="col-md-12 logo-sec">
                                    <span class="coll-3 input-effect"><img src="{{URL::to('/img/logo-l.png')}}">
                                        <div class="logo-cnt">
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text" name="header_service_provider" value="{{$company->fname.' '.$company->lname.'/'.$company->phone}}" placeholder="Service Provider Number/Name"><span class="focus-border"></span></span>
                                        </div>
                                </div>
                                <div class="col-md-12">
                                    <h1>Response to Proposal</h1>
                                    <h3>Project Name: <span class="coll-3 input-effect"><input class="effect-18 project-name" value="{{$invitation->title}}" type="text" name="header_project_name" placeholder="Project Name"><span class="focus-border"></span></span></h3>
                                    <h3>Prepared for: <span class="coll-3 input-effect"><input class="effect-18 customer-name" type="text" value="{{$invitation->senderfname.' '.$invitation->senderlname}}" name="header_customer_name" placeholder="Customer Name"><span class="focus-border"></span></span></h3>
                                    <p class="date">Date: 08/08/2018</p>
                                </div>
                        </div>
                    </div>
                    <div class="main-cnt-sec container-fluid">
                            <div class="row">
                                <div class="col-md-12 overview-sec">
                                    <h2>Overview of <span class="coll-3 input-effect"><input class="effect-18" class="effect-18" type="text" name="over_service_provider_title"  value="{{$company->fname.' '.$company->lname}}" placeholder="Service Provider"><span class="focus-border"></span></span></h2>
                                    <p>Thank you for connecting with <span class="coll-3 input-effect"><span class="coll-3 input-effect"><input class="effect-18" class="effect-18" type="text" name="over_service_provider"  value="{{$company->fname.' '.$company->lname}}" placeholder="Service Provider Name"><span class="focus-border"></span></span>for your installation/repairs project needs. 
                                            We specialize in <span class="coll-3 input-effect"><input class="effect-18" type="text" name="over_domain_area" placeholder="Domain Area"><span class="focus-border"></span></span> servicing clients around <span class="coll-3 input-effect"><input class="effect-18" type="text" name="over_city_zip" value="{{$company->city.'('.$company->pin_code.')'}}" placeholder="Zip Code City"><span class="focus-border"></span></span> area. We pride ourselves in following best practices and methodology delivering <span class="coll-3 input-effect"><input class="effect-18" type="text" name="over_service_provider2" value="{{$company->fname.' '.$company->lname}}" placeholder="Service Provider Name"><span class="focus-border"></span></span> related services. We believe we can make a difference and provide solution for <span class="coll-3 input-effect"><input class="effect-18 customer-name" type="text" name="over_customer" value="{{$invitation->senderfname.' '.$invitation->senderlname}}" placeholder="customer"><span class="focus-border"></span></span> @ <span class="coll-3 input-effect"><input class="effect-18 customer-address" type="text" name="over_address" placeholder="Address Location"><span class="focus-border"></span></span>  with the best cost saving and quality service if you select us for your project</p>
                                </div>
                                <div class="col-md-12 Assessment-sec">
                                    <h2>Assessment</h2>
                                    <p>As an industry leader within the <span class="coll-3 input-effect"><input class="effect-18" name="assessment_domain" placeholder="Domain" type="text"><span class="focus-border"></span></span>, <span class="coll-3 input-effect"><input class="effect-18" name="assessment_service_provider1" value="{{$company->fname.' '.$company->lname}}" placeholder="Service Provider/Profile" type="text"><span class="focus-border"></span></span> provides qualified professionals to tackle <span class="coll-3 input-effect"><input class="effect-18 customer-name" type="text" name="assessment_customer" value="{{$invitation->senderfname.' '.$invitation->senderlname}}" placeholder="customer"><span class="focus-border"></span></span>challenges and resolve any problem with various task(s) listed in your <span class="coll-3 input-effect"><input class="effect-18 project-name" type="text" value="{{$invitation->title}}" name="assessment_project_name" placeholder="project name"> <span class="focus-border"></span></span>project. We are available for site assessment and review of more details you may not have covered in your initial request for proposal. <span class="coll-3 input-effect"><input class="effect-18" type="text"  name="assessment_service_provider2" value="{{$company->fname.' '.$company->lname}}" placeholder="Service provider"><span class="focus-border"></span></span> guarantee success in your project because we are:</p>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Requirement</th>
                                                    <th>Response</th>
                                                    <th>Comment</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Covered by Insurance/Insured</td>
                                                    <td>
                                                        <label class="switch">
                                                            <input class="switch-input" name="Insured" type="checkbox" />
                                                            <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <span class="coll-3 input-effect"><input class="effect-18" type="text" name="Insured_comment" placeholder=""><span class="focus-border"></span></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Availability of resource and Equipment to deliver</td>
                                                    <td>
                                                        <label class="switch">
                                                            <input class="switch-input" name="deliver" type="checkbox" />
                                                            <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <span class="coll-3 input-effect"><input class="effect-18" type="text" name="deliver_comment" placeholder=""><span class="focus-border"></span></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Available for Interview </td>
                                                    <td>
                                                        <label class="switch">
                                                            <input class="switch-input" name="interview" type="checkbox" />
                                                            <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <span class="coll-3 input-effect"><input class="effect-18" type="text" name="interview_comment" placeholder=""><span class="focus-border"></span></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Licensed and Bonded     </td>
                                                    <td>
                                                        <label class="switch">
                                                            <input class="switch-input" name="bonded" type="checkbox" />
                                                            <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <span class="coll-3 input-effect"><input class="effect-18" type="text" name="bonded_comment" placeholder=""><span class="focus-border"></span></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Previous job reference</td>
                                                    <td>
                                                        <label class="switch">
                                                            <input class="switch-input" name="job_reference" type="checkbox" />
                                                            <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span>
                                                        </label>							</td>
                                                    <td>Protect Customer privacy</td>
                                                </tr>
                                                <tr>
                                                    <td>Community Support hours</td>
                                                    <td>
                                                        <label class="switch">
                                                            <input class="switch-input" name="support_hrs" type="checkbox" />
                                                            <span class="switch-label" data-on="Yes" data-off="No"></span> <span class="switch-handle"></span>
                                                        </label>
                                                    </td>
                                                    <td>60 hours</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12 Implementationnt-sec">
                                    <h2>Implementation and Execution Strategy</h2>
                                    <p>Our execution strategy incorporates proven methodologies, extremely qualified personnel, and a highly responsive approach to managing deliverables. We guarantee daily/weekly updates depending on mutual agreement between  <span class="coll-3 input-effect"><input class="effect-18" type="text"  name="company_name" value="{{$company->shop_name}}" placeholder="Service provider company"><span class="focus-border"></span></span> and <span class="coll-3 input-effect"><input class="effect-18 customer-name" type="text" name="stategy_customer_name" value="{{$invitation->senderfname.' '.$invitation->senderlname}}" placeholder="Customer"><span class="focus-border"></span></span></p>
                                </div>
                                <div class="col-md-12 Expected-sec">
                                    <h2>Expected Result</h2>
                                    <p>After complete delivery of this project  <span class="coll-3 input-effect"><input class="effect-18" type="text" name="result_service_provider1" value="{{$company->fname.' '.$company->lname}}" placeholder="service provider"><span class="focus-border"></span></span> guarantees to meet the expectations of <span class="coll-3 input-effect"><input class="effect-18 customer-name"  type="text" name="result_customer1"  value="{{$invitation->senderfname.' '.$invitation->senderlname}}" placeholder="Customer"><span class="focus-border"></span></span> within the framework of the requirement for this <span class="coll-3 input-effect"><input class="effect-18 project-name"  value="{{$invitation->title}}" type="text" name="result_project_name1" placeholder="project name"><span class="focus-border"></span></span></p>
                                </div>
                                <div class="col-md-12 Project-sec">
                                    <h2>Project Team</h2>
                                    <p>Our delivery team is based out <span class="coll-3 input-effect"><input class="effect-18" type="text" name="team_city_zip" value="{{$company->city.' ('.$company->pin_code.')'}}" placeholder="Zip code City"><span class="focus-border"></span></span>. Our team member are always available to address your ongoing project concerns or send us a message through Home trumpeter chat room/email etc…</p>
                                </div>
                                <div class="col-md-12 PDeliverables-sec">
                                    <h2>Project Deliverables</h2>
                                    <p><span class="coll-3 input-effect"><input class="effect-18" type="text" name="deliverable_service_provider" value="{{$company->fname.' '.$company->lname}}" placeholder="Service provider"><span class="focus-border"></span></span>will deliver this project within <span class="coll-3 input-effect"><input class="effect-18  proposal-start-date" type="text" name="deliverable_start_date" value="{{date('d M Y', strtotime($invitation->start_date))}}" placeholder="Proposed Start Date"><span class="focus-border"></span></span> and <span class="coll-3 input-effect"><input class="effect-18 proposal-end-date" type="text"  name="deliverable_end_date" value="{{date('d M Y', strtotime($invitation->end_date))}}" placeholder="proposed end date"><span class="focus-border"></span></span> These dates are best guess estimates and are subject to change under a situation or circumstances beyond the control of the <span class="coll-3 input-effect"><input class="effect-18" type="text" name="deliverable_service_provider2" value="{{$company->fname.' '.$company->lname}}" placeholder="service provider"><span class="focus-border"></span></span> and <span class="coll-3 input-effect"><input class="effect-18 customer-name" type="text" name="deliverable_customer" value="{{$invitation->senderfname.' '.$invitation->senderlname}}" placeholder="customer"><span class="focus-border"></span></span>.One of such situations include inclement weather condition. The project deliverable includes the following tasks:</p>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Task</th>
                                                    <th>Description</th>
                                                    <th>Estimate</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>
                                                        <span class="coll-3 input-effect"><input class="effect-18" type="text" name="list_desc1" placeholder="Items listed by the Customer"><span class="focus-border"></span></span>
                                                    </td>
                                                    <td>
                                                        <span class="coll-3 input-effect"><input class="effect-18" type="text" name="list_estimate1" placeholder=""><span class="focus-border"></span></span>	
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>
                                                        <span class="coll-3 input-effect"><input class="effect-18" type="text" name="list_desc2" placeholder="Items listed by the Customer"><span class="focus-border"></span></span>
                                                    </td>
                                                    <td>
                                                        <span class="coll-3 input-effect"><input class="effect-18" type="text" name="list_estimate2" placeholder=""><span class="focus-border"></span></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-12 Pricing-sec">
                                    <h2>Pricing and Billing:</h2>
                                    <p><span class="coll-3 input-effect"><input class="effect-18" type="text" name="billing_service_provider1" value="{{$company->fname.' '.$company->lname}}" placeholder="Service provider"><span class="focus-border"></span></span> collect payment through HomeTrumpeter platform.  We charge our client <span class="coll-3 input-effect"><input class="effect-18" type="text" name="billing_hrs" placeholder="/hr"><span class="focus-border"></span></span>.<span class="coll-3 input-effect"><input class="effect-18 customer-name" type="text" name="billing_customer_name" value="{{$invitation->senderfname.' '.$invitation->senderlname}}" placeholder="Customer"><span class="focus-border"></span></span> will initiate payment after satisfactory inspection of service as agreed by the two parties The rate listed above is the estimate for the service discussed. Estimates are subject to change if additional tasks are required before or during project execution. The cost associated with materials and parts shall be discussed before the commencement of the project.</p>
                                </div>
                                <div class="col-md-12 terms-sec">
                                    <h2>Terms and Agreement</h2>
                                    <p>Prior to any contractual agreement, the content of this proposals is subject to change. This change is a joint effort with mutual understanding between <span class="coll-3 input-effect"><input class="effect-18 customer-name" type="text" name="agreement_customer" value="{{$invitation->senderfname.' '.$invitation->senderlname}}" placeholder="customer"><span class="focus-border"></span></span>  and <span class="coll-3 input-effect"><input class="effect-18" type="text" name="agreement_service_provider" value="{{$company->fname.' '.$company->lname}}" placeholder="Service provider"><span class="focus-border"></span></span></p>
                                </div>
                                <div class="col-md-12 Acceptance-sec">
                                    <h2>Acceptance:</h2>
                                    <div class="Acceptance-input">
                                        <div class="inpt-sec">
                                            <label>Name:</label>
                                            <span class="coll-3 input-effect"><input class="effect-18 customer-name" type="text" value="{{$invitation->senderfname.' '.$invitation->senderlname}}" name="acceptance_customer_name" placeholder="Customer"><span class="focus-border"></span></span>
                                        </div>
                                        <div class="inpt-sec">
                                            <span class="coll-3 input-effect"><input class="effect-18" type="text" name="acceptance_service_provider" value="{{$company->fname.' '.$company->lname}}" placeholder="Service Provider"><span class="focus-border"></span></span>
                                        </div>
                                    </div>
                                    <div class="inpt-sec data-pick">
                                        <label>Date:</label>
                                        <input type="text" name="date" id="datepicker">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" >Send Response</button>
                    </div>
                </form>
</div>
                @include('footer')
                </body>
</html>