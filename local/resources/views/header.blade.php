<?php
use Illuminate\Support\Facades\Route;
$currentPaths= Route::getFacadeRoot()->current()->uri();	
$url = URL::to("/");
$setts = DB::table('settings')->where('id',1)->first();?>	

 <header>
        <div class="header header2">
            <div class="col-md-12">
						<a class="" href="{{URL::to('/')}}">
	  @if(!empty($setts->site_logo))
		<h1 class="logo"> <img src="{{URL::to('/local/images/settings/'.$setts->site_logo)}}" border="0" alt="{{$setts->site_name}}" /></h1>
		   @else
			 <h1 class="logo"> {{$setts->site_name}}</h1>
		  @endif
			</a>
			@if(Auth::guest())
				<ul class="login_sign">
				<li><a href="{{URL::to('/register')}}">Sign Up</a></li>
				<li><a href="{{URL::to('/login')}}">Login</a></li>
				</ul>
				<button type="button" data-toggle="modal" data-target="#subscrib_popup" class="btn_fix subscribe_bt">Subscribe</button>
				@else
								<img class="toggle_menu" onclick="openNav()" src="{{URL::to('/img/side_icon.png')}}">
								<div id="mySidenav" class="sidenav" style="right: -290px;">
                <div class="back_lyer_col" onclick="closeNav()"></div>   
									<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
									<li><a href="{{URL::to('/dashboard')}}">MY PROFILE</a></li>
				@if(Auth::user()->admin==0)
					
						<li><a href="{{URL::to('/my_bookings')}}">MY BOOKINGS</a></li>
						<li><a href="{{URL::to('/proposals')}}">PROPOSALS</a></li>
					     
				@endif     
				@if(Auth::user()->admin==2)

		<?php	$sellmail = Auth::user()->email;
    	 $shcount = DB::table('shop')
		 ->where('seller_email', '=',$sellmail)
		 ->count(); ?>
		  
				<li <?php if(Auth::user()->status == 0){?>class="disabled"<?php } ?>><a href="{{URL::to('myorder')}}">My Order</a></li>
				<li <?php if(Auth::user()->status == 0){?>class="disabled"<?php } ?>><a href="{{URL::to('invitation')}}">Jobs Invitation</a></li>
				<li <?php if(Auth::user()->status == 0){?>class="disabled"<?php } ?>><a href="<?php if(empty($shcount)){?><?php echo $url;?>/addshop<?php } else { ?><?php echo $url;?>/shop<?php } ?>">My Shop</a></li>
				<li <?php if(Auth::user()->status == 0){?>class="disabled"<?php } ?>><a href="<?php echo $url;?>/services" <?php if(empty($shcount)){?>class="disabled"<?php } ?>>My Services</a></li>
				<li <?php if(Auth::user()->status == 0){?>class="disabled"<?php } ?>><a href="<?php echo $url;?>/wallet" <?php if(empty($shcount)){?>class="disabled"<?php } ?>>Wallet</a></li>
				@endif
				<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout</a></li>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
				<li><button type="button" data-toggle="modal" data-target="#subscrib_popup" class="btn_fix subscribe_bt repon">Subscribe</button></li>
				</div>
				@endif
            </div>
        </div>
	</header>
