
        @include('style')
        <!-- fixed navigation bar -->
        @include('header')
        <div class="heading_login">
            <h1>My Order</h1>    
        </div>	
        <div class="login_form">
            <div class="container">
                <div class="dashboard">
                    <div class="col-md-12"> 
                        <div class="cart">
                            <div class="main_table_sec nth_table">
                                <div class="table-responsive table_size_fix">
                                    <div class="scrol_fix_auto">
                                        <table class="table table-borderless">
                                            <thead>
                                                <tr>
                                                    <th>Sno</th>
                                                    <th>Shop Name</th>
                                                    <th>Services Name</th>
                                                    <th>Booking date</th>
                                                    <th>Booking time</th>
                                                    <th>Booking Note</th>
                                                    <th>User Name</th>
                                                    <th>User Email</th>
                                                    <th>User Phone No</th>
                                                    <th>Total Amount</th>
                                                    <th>Status</th>
                                                    <th>Is Completed</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sno = 0;
                                                foreach ($booking as $viewbook) {
                                                    $sno++;
                                                    $booking_time = $viewbook->booking_time;
                                                    if ($booking_time > 12) {
                                                        $final_time = $booking_time - 12;
                                                        $final_time = $final_time . "PM";
                                                    } else {
                                                        $final_time = $booking_time . "AM";
                                                    }
                                                    $ser_id = $viewbook->services_id;
                                                    $sel = explode(",", $ser_id);
                                                    $lev = count($sel);
                                                    $ser_name = "";
                                                    $sum = "";
                                                    $price = "";
                                                    for ($i = 0; $i < $lev; $i++) {
                                                        $id = $sel[$i];
                                                        $fet1 = DB::table('subservices')->where('subid', $id)->get();
                                                        $ser_name.=$fet1[0]->subname . '<br>';
                                                        $ser_name.=",";
                                                        $ser_name = trim($ser_name, ",");
                                                    }
                                                    $bookid = $viewbook->book_id;
                                                    $newbook = DB::table('booking')->where('book_id', $bookid)->get();
                                                    $userdetail = DB::table('users')->where('id', $newbook[0]->user_id)->get();
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $sno; ?></td>
                                                        <td><?php echo $viewbook->shop_name; ?></td>
                                                        <td><?php echo $ser_name; ?></td>
                                                        <td><?php echo $viewbook->booking_date; ?></td>
                                                        <td><?php echo $final_time; ?></td>
                                                        <td><?php echo $viewbook->booking_note; ?></td>
                                                        <td><?php echo $userdetail[0]->name; ?></td>
                                                        <td><?php echo $viewbook->user_email; ?></td>
                                                        <td><?php echo $userdetail[0]->phone; ?></td>
                                                        <td><?php echo $viewbook->total_amt . ' ' . $setting[0]->site_currency; ?></td>
                                                        <?php
                                                        if ($newbook[0]->status == "pending") {
                                                            $color = "#F31C0A";
                                                        } else if ($newbook[0]->status == "paid") {
                                                            $color = "#0DE50D";
                                                        } else if ($newbook[0]->status == "failed") {
                                                            $color = "#FB8C00";
                                                        } else {
                                                            $color = "#F31C0A";
                                                        }
                                                        if ($newbook[0]->is_completed == 0) {
                                                            $color1 = "#F31C0A";
                                                            $is_completed = 'Pendding';
                                                        } else if ($newbook[0]->is_completed == 1) {
                                                            $color1 = "#0DE50D";
                                                            $is_completed = 'Completed';
                                                        } else if ($newbook[0]->is_completed == 2) {
                                                            $color1 = "#FB8C00";
                                                            $is_completed = 'Dispute';
                                                        } else {
                                                            $color1 = "#F31C0A";
                                                            $is_completed = 'pending';
                                                        }
                                                        ?> 
                                                        <td style="color:<?php echo $color; ?>;"><?php echo $newbook[0]->status; ?></td>
                                                        <td style="color:<?php echo $color1; ?>;"><?php echo $is_completed; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>   
                                </div>
                                <!-- <div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate"><a class="paginate_button previous disabled" aria-controls="myTable" data-dt-idx="0" tabindex="0" id="myTable_previous">Previous</a><span><a class="paginate_button current" aria-controls="myTable" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="3" tabindex="0">3</a></span><a class="paginate_button next" aria-controls="myTable" data-dt-idx="7" tabindex="0" id="myTable_next">Next</a><div></div></div> -->
                            </div>       
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
        <!-- slider -->
        @include('footer')