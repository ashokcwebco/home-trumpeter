 
<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
        <!-- fixed navigation bar -->
        @include('header')
        <meta name="_token" content="{{csrf_token()}}" />

 <div class="heading_login">
            <h1>Sent Invitation</h1>    
        </div>	
        <div class="login_form">
            <div class="container">
                <div class="dashboard">
                    <div class="col-md-12"> 
                        <div class="cart">
                            <div class="main_table_sec invita_nth">
                                 <div class="alert alert-success" style="display:none">
                                </div>
                                <div class="alert alert-danger" style="display:none">
                                </div>
                                <div class="table-responsive table_size_fix">
                                    <div class="scrol_fix_auto">
                                        <table class="table table-borderless">
                                            <thead>
                                                <tr>
                                                    <th>Job Title</th>
                                                    <th>Service Provider</th>
                                                    <th>Created At</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $initeIds = array(); ?>
                                                @foreach ($invitations as $key=>$invitation)
                                                    <tr>
                                                        <?php $initeIds[] = $invitation->invite_id; ?>
                                                        <td>{{$invitation->title }}</td>
                                                        <td>{{$invitation->shop_name}} </td>
                                                        <td>{{ date('d M Y', strtotime($invitation->created_at))}}</td>
                                                        <td>
                                                        @if($invitation->status ==0)
                                                        <span style="color:#f2bc59;">Pending</span>
                                                        @elseif($invitation->status ==1)
                                                        <span style="color:#0DE50D;">Accepted</span>
                                                            <a href="javascript:;" class="discuss-win" data-title="{{$invitation->title}}" data-id="{{$invitation->invite_id}}"> <i class="fa fa-commenting-o" style="font-size:24px;color:#01CC79"></i></a>
                                                        @else
                                                        <span style="color:#CF2335;">Declined</span>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                              
                                              
                                            </tbody>
                                            <input type="hidden" id="inviteIDS" value="{{ implode(",",$initeIds)}}">
                                        </table>
                                    </div>   
                                </div>
                                <!-- <div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate"><a class="paginate_button previous disabled" aria-controls="myTable" data-dt-idx="0" tabindex="0" id="myTable_previous">Previous</a><span><a class="paginate_button current" aria-controls="myTable" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="3" tabindex="0">3</a></span><a class="paginate_button next" aria-controls="myTable" data-dt-idx="7" tabindex="0" id="myTable_next">Next</a><div></div></div> -->
                            </div>       
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
    <div class="discuss-main-win" >
     
    </div>
    @include('footer')
</body>
<script>
$('.discuss-win').on('click',function(){
    loaddefault($(this).attr('data-id'));
    $('.discuss-main-win').append('<div class="col-sm-2 frame" id="'+$(this).attr('data-id')+'">'+
            '<span class="toolbar"><span>'+$(this).attr('data-title')+'</span> <a href="javascript:;" id="close"><i class="fa fa-times" aria-hidden="true"></i></a></span>'+
            '<ul id="rfp-dis-'+$(this).attr('data-id')+'" class="rfp-dis"></ul>'+
            '<div>'+
                '<div class="msj-rta macro" style="margin:auto">'+                        
                    '<div class="text text-r" style="background:whitesmoke !important">'+
                        '<form id="file-form" action="" method="POST">'+
                        '<input type="file"  rel="'+$(this).attr('data-id')+'" id="file-select" name="photos[]" multiple/>'+
                        '</form>'+
                        '<input class="mytext" rel="'+$(this).attr('data-id')+'" placeholder="Type a message"/>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>');
});


$(document).on('change','#file-select',function(e){
    var fileSelect = document.getElementById('file-select');
    var files = fileSelect.files;
    var formData = new FormData();
    formData.append('inviteId', fileSelect.getAttribute('rel'));
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        // if (file.type.match('image.*')) {
        //     debugger;
        //     continue;
        // }
        formData.append('photos', file, file.name);
        }
        src = "{{ route('sendfile') }}";
       
        var xhr = new XMLHttpRequest();
        xhr.upload.addEventListener("progress", function(evt){
            if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                console.log(percentComplete);
            }
       }, false);
        xhr.open('POST', src, true);
        xhr.setRequestHeader('X-CSRF-Token', $('input[name="_token"]').val());
        xhr.onload = function () {
            if (xhr.status === 200) {
                res = JSON.parse(xhr.response);
                if(res.data.length>0){
                    for(var i=0; i< res.data.length; i++ ){
                       var whoisUser =  (res.data[i].user_Id == AuthUser)?'me':'service';
                        insertChat(whoisUser, res.data[i].message,0,res.data[i].invite_id);
                    }
                } 
            } else {
            }
        };
        xhr.send(formData);
  
 });
setInterval(function(){ loadDataIntervel() }, 3000);


function loadDataIntervel(){
    src = "{{ route('loaddataintervel') }}";
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
        $.ajax({
            type: "POST",
            url:src,
            data: {inviteId:$('#inviteIDS').val()},
            success: function( res ) {
                res = JSON.parse(res);
                if(res.data.length>0){
                    for(var i=0; i< res.data.length; i++ ){
                       var whoisUser =  (res.data[i].user_Id == AuthUser)?"me":"service";
                        insertChat(whoisUser, res.data[i].message,0,res.data[i].invite_id);
                    }
                }  
            }
        });
}


$(document).on('click','#close',function(){
    $('#'+$(this).closest('.frame').attr('id')).css('display','none');
});
var AuthUser = '<?php echo Auth::user()->id; ?>';
var me = {};
var you = {};

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}            

//-- No use time. It is a javaScript effect.
function insertChat(who, text, time = 0,chatId){
    var control = "";
    var date = formatAMPM(new Date());
    
    if (who == "me"){
        
        control = '<li style="width:100%">' +
                        '<div class="msj macro">' +
                            '<div class="text text-l">' +
                                '<p>'+ text +'</p>' +
                                '<p><small>'+date+'</small></p>' +
                            '</div>' +
                        '</div>' +
                    '</li>';                    
    }else{
        control = '<li style="width:100%;">' +
                        '<div class="msj-rta macro">' +
                            '<div class="text text-r">' +
                                '<p>'+text+'</p>' +
                                '<p><small>'+date+'</small></p>' +
                            '</div>' +
                        '<div class="avatar" style="padding:0px 0px 0px 10px !important"></div>' +                                
                  '</li>';
    }
    setTimeout(
        function(){                        
            $("#rfp-dis-"+chatId).append(control);

        }, time);
    
}

function resetChat(){
    $("#rfp-dis").empty();
}


function save(msg,inviteId){
        src = "{{ route('pushmessage') }}";
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
        $.ajax({
            type: "POST",
            url:src,
            data: {msg:msg,inviteId:inviteId},
            success: function( res ) {
                res = JSON.parse(res);
                if(res.data.length>0){
                    for(var i=0; i< res.data.length; i++ ){
                       var whoisUser =  (res.data[i].user_Id == AuthUser)?'me':'service';
                        insertChat(whoisUser, res.data[i].message,0,res.data[i].invite_id);
                    }
                }  
            }
        });
}

$(document).on("keyup",".mytext", function(e){

    if (e.which == 13){
        
        var text = $(this).val();
        var id = $(this).attr('rel');
        if (text !== ""){
            save(text,id);
            insertChat("me", text);              
            $(this).val('');
        }
    }
});


function loaddefault(inviteId){
        src = "{{ route('getmsgs') }}";
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
        $.ajax({
            type: "POST",
            url:src,
            data: {inviteId:inviteId},
            success: function( res ) {
                res = JSON.parse(res);
                if(res.data.length>0){
                    for(var i=0; i< res.data.length; i++ ){
                       var whoisUser =  (res.data[i].user_Id == AuthUser)?"me":"service";
                        insertChat(whoisUser, res.data[i].message,0,res.data[i].invite_id);
                    }
                }  
            }
        });
}

//-- Clear Chat
resetChat();
//-- NOTE: No use time on insertChat.
</script>
</html>