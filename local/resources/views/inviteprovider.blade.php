 
<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
    </head>
    <body>
        <!-- fixed navigation bar -->
        @include('header')
        <meta name="_token" content="{{csrf_token()}}" />
        <div class="heading_login">
            <h1>Send Invitation</h1>    
        </div>	
        <div class="login_form">
            <div class="container">
                <div class="dashboard">
                    <div class="col-md-12"> 
                        <div class="cart">
                            <div class="main_table_sec ">
                                <div class="alert alert-success" style="display:none">
                                </div>
                                <div class="alert alert-danger" style="display:none">
                                </div>
                                <div class="col-md-12 text-right">
                                    <a href="javascript:;" id="invite"  class="login_btn servic_bnt">Send Invitation</a>
                                </div>
                                <input type="hidden" id="proposalId" value="{{$proposal}}" > 
                                <div class="table-responsive table_size_fix">
                                    <div class="scrol_fix_auto">
                                        <table class="table table-borderless">
                                            <thead>
                                                <tr>
                                                    <th style="width:30px;"><input type="checkbox" id="checkAll"></th>
                                                    <th>Service Provider</th>
                                                    <th>Mob. Number</th>
                                                    <th>Service Time (open/close)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($shops as $key=>$shop)
                                                <tr>
                                                    <th><input type="checkbox" name="shop[]" value="{{$shop->id}}" ></th>
                                                    <td>{{$shop->shop_name }}</td>
                                                    <td>{{$shop->shop_phone_no }} </td>
                                                    <td>{{ $shop->start_time.':00 / '. $shop->end_time.':00' }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>   
                                </div>
                                <!-- <div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate"><a class="paginate_button previous disabled" aria-controls="myTable" data-dt-idx="0" tabindex="0" id="myTable_previous">Previous</a><span><a class="paginate_button current" aria-controls="myTable" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="3" tabindex="0">3</a></span><a class="paginate_button next" aria-controls="myTable" data-dt-idx="7" tabindex="0" id="myTable_next">Next</a><div></div></div> -->
                            </div>       
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
        <script>
            jQuery(document).ready(function () {
          
            
                $('#checkAll').click(function (e) {
                    if (e.currentTarget.checked == false) {
                        
                        $('input').prop('checked', false);
                    } else {
                        $('input').prop('checked', true);
                    }
                });
                $('#invite').on('click', function (e) {
                    var shopValue = [];
                    $('input[type="checkbox"]:checked').each(function (e) {
                        if ($(this).attr('id') != "checkAll") {
                            shopValue.push($(this).val());
                        }
                    });
                    if (shopValue.length < 1){
                        $('.alert-danger').css('display', 'block');
                        $('.alert-danger').text('Sorry Please select atleast service provider');
                        return false
                    }
                    src = "{{ route('sendInvitation') }}";
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: src,
                        data: {proposal: $('#proposalId').val(), shops: shopValue},
                        success: function (data) {
                            data = JSON.parse(data);
                            if (data.status == 'success') {
                                $('.alert-success').css('display', 'block');
                                $('.alert-success').text('invitation sent successfully..Thanks');
                            } else {
                                $('.alert-danger').css('display', 'block');
                                $('.alert-danger').text('sorry invitation not sent please send again..');
                            }
                        }
                    });
                });
            });
        </script>
        @include('footer')
    </body>
</html>