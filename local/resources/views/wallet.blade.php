<!DOCTYPE html>
<html lang="en">
    <head>
        @include('style')
        <script type="text/javascript">
            function withdraw_check(str)
            {
                if (str == "stripe")
                {
                    document.getElementById("stripe").style.display = "block";
                }
            }
        </script>
    </head>
    <body>
        <!-- fixed navigation bar -->
        @include('header')
        <!-- slider -->
        <div class="heading_login">
            <h1>Wallet</h1>    
        </div>	
        <div class="login_form">
            <div class="container">
                <div class="dashboard">
                    <div class="col-md-12"> 
                        <div class="cart">
                            <form class="form-large" action="{{ route('wallet') }}" accept-charset="UTF-8" id="formID" method="post">
                                {{ csrf_field() }}
                                <div class="service_form">
                                    <input type="hidden" id="shop_id" name="shop_id" value="{{$shop_id}}">
                                    <input type="hidden" name="min_with_amt" value="{{$setting->withdraw_amt}}">
                                    <div class="col-md-3">
                                        <div class="custom_inp input-effect">
                                            <input type="text" class="effect-16" id="shop_balance" name="shop_balance" readonly value="{{$with_count!= 0?$shop_balance:$bal}}">
                                            <label>Shop Balance [ {{$setting->site_currency}} ] </label>    
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="custom_inp input-effect">
                                            <input type="text" class="effect-16 validate[required] text-input" id="withdraw_amt" name="withdraw_amt">
                                            <label>Withdraw Amount </label>    
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="control-group">    
                                            <div class="select">
                                                <select id="withdraw_mode" name="withdraw_mode" class="validate[required]" onchange="javascript:withdraw_check(this.value);">
                                                    <option value="">Withdraw Option</option>
                                                    @for($i = 0; $i <  count(explode(",", $setting->withdraw_option)); $i++)
                                                    <option value="{{explode(",", $setting->withdraw_option)[$i]}}" >{{explode(",", $setting->withdraw_option)[$i]}}</option>
                                                    @endfor
                                                </select>
                                                <div class="select__arrow"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3" id="stripe" style="display:none;">
                                        <div class="custom_inp input-effect">
                                            <input type="text" class="effect-16 validate[required] text-input" id="stripe_id" name="stripe_id">
                                            <label>Enter Stripe Email ID </label>    
                                            <span class="focus-border"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="submit" class="login_btn servic_bnt" style="background: #f6cd53;" value="Save">
                                    </div>
                                </div>
                            </form>
                            @if(!empty($with_count))
                            <div class="main_table_sec walet_nth">
                                <h2>Pending Withdrawal </h2>
                                <div class="table-responsive table_size_fix">
                                    <div class="scrol_fix_auto">
                                        <table class="table table-borderless">
                                            <thead>
                                                <tr>
                                                    <th>SNo</th>
                                                    <th>Withdraw Amount</th>
                                                    <th>Withdraw Mode</th>
                                                    <th>Stripe Id</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($withdraws as $key => $row)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td> {{$row->withdraw_amt .' '.$setting->site_currency}}</td>
                                                    <td>{{ $row->withdraw_mode}}</td>	
                                                    <td>{{$row->stripe_id}}</td>
                                                    <td>{{$row->withdraw_status}}</td>											
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>   
                                </div>
                                <!-- <div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate"><a class="paginate_button previous disabled" aria-controls="myTable" data-dt-idx="0" tabindex="0" id="myTable_previous">Previous</a><span><a class="paginate_button current" aria-controls="myTable" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="3" tabindex="0">3</a></span><a class="paginate_button next" aria-controls="myTable" data-dt-idx="7" tabindex="0" id="myTable_next">Next</a><div></div></div> -->
                            </div>  
                            <div class="main_table_sec walet_nth">
                                <h2>Completed Withdrawal </h2>
                                <div class="table-responsive table_size_fix">
                                    <div class="scrol_fix_auto">
                                        <table class="table table-borderless">
                                            <thead>
                                                <tr>
                                                    <th>SNo</th>
                                                    <th>Withdraw Amount</th>
                                                    <th>Withdraw Mode</th>
                                                    <th>Stripe Id</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($withdraws_cc as $key =>$row)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td> {{$row->withdraw_amt .' '.$setting->site_currency}}</td>
                                                    <td>{{ $row->withdraw_mode}}</td>	
                                                    <td>{{$row->stripe_id}}</td>
                                                    <td>{{$row->withdraw_status}}</td>	
                                                </tr>
												@endforeach
                                            </tbody>
                                        </table>
                                    </div>   
                                </div>
                                <!-- <div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate"><a class="paginate_button previous disabled" aria-controls="myTable" data-dt-idx="0" tabindex="0" id="myTable_previous">Previous</a><span><a class="paginate_button current" aria-controls="myTable" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="2" tabindex="0">2</a><a class="paginate_button " aria-controls="myTable" data-dt-idx="3" tabindex="0">3</a></span><a class="paginate_button next" aria-controls="myTable" data-dt-idx="7" tabindex="0" id="myTable_next">Next</a><div></div></div> -->
                            </div>  
                            @endif 
                        </div>
                    </div>
                </div> 
            </div>
        </div>    
        @include('footer')
        <?php if (session()->has('message')) { ?>
            <script type="text/javascript">
                alert("<?php echo session()->get('message'); ?>");
            </script>
        </div>
    <?php } ?>
</body>
</html>