<?php

namespace Responsive\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Mail;
use Auth;
use PDF;
use PhpOffice\PhpWord\Autoloader;
use PhpOffice\PhpWord\Settings;
use Illuminate\Support\Facades\Validator;
class ProposalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function __construct()
    {
       $this->middleware('auth');
    }


    function index(){
        $proposals =  DB::table('proposals')->where('user_id',Auth::user()->id)->get();
        foreach($proposals as $key=>$proposal){
            $proposals[$key]->serviceName = DB::table('subservices')->whereIn('subid',explode(",", $proposal->services))->get();
        }
        $data = array("proposals"=>$proposals);
        return view('myproposal')->with($data);
    }

    function add_proposal(){
        $services = DB::table('subservices')->get();
        $data = array("subservices"=>$services);
        return view('perposal')->with($data);
    }

    function save_proposal(Request $request){
        $rules = array(
            'services' => 'required',
            'title' => 'required|max:255',
            'description' => 'required'
        );
        $messages = array(
            'services' => 'Please select services',
            'title' => 'The :attribute field must only be letters',
            'description' => 'Please type description'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $failedRules = $validator->failed();
            return back()->withErrors($validator);
        } else {
            $services = implode(',',Input::get('services'));
            $description = Input::get('description');
            $user_id = Auth::user()->id;
            $data = array(
                "user_id"=> $user_id,
                "title"=> Input::get('title'),
                "services"=>$services,
                "description"=> $description, 
                "status"=>1,
                "start_date"=> date('Y-m-d H:i:s',strtotime(Input::get('startDate'))),
                "end_date" => date('Y-m-d H:i:s',strtotime(Input::get('endDate'))),
                'created_at'=>date('Y-m-d H:i:s'));
            DB::table('proposals')->insert($data);
            return back()->with('success', 'proposal saved successfully');
        }
    }

    function send_perposal(Request $request){

      $shops = DB::table('shop')->whereIn('id',$request->shops)->get();
      $proposal = DB::table('proposals')
      ->leftJoin('users','users.id','=','proposals.user_id')
      ->select('proposals.title', 'proposals.description', 'users.fname', 'users.lname')
      ->where('proposals.perposal_id',$request->proposal)->first();
        $totalProviderSelected = count($shops);
        $sentInvitations = array();
        if($shops){
            $sentStatus = 0;
            foreach($shops as $shop):
                    Mail::send('proposalemailtmplate', ['shop_name' => $shop->shop_name, 'job_title' => $proposal->title, 'desc' =>$proposal->description, 'customer_name' => $proposal->fname.' '.$proposal->lname], 
                    function ($message) use ($shop,$proposal)
                    {
                        $message->subject('invitation from '.$proposal->fname.' '.$proposal->lname);
                        $message->from('info@hometrumpeter.com', 'Hometrumpeter');
                        $message->to($shop->seller_email); 
                    });
                    $sentStatus++;
                $sentInvitations[] = array('sender_id'=>Auth::user()->id,'receiver_id' =>$shop->user_id,'proposal_Id'=>$request->proposal,'status'=>0,'created_at'=>date('Y-m-d H:i:s'));

            endforeach;
            DB::table('sent_invitations')->insert($sentInvitations);
        }
        if($totalProviderSelected == $sentStatus)
             echo json_encode(array('status'=>'success'));
        else
             echo json_encode(array('status'=>'fail'));
        
    }

    function get_invitation_provider($proposal_id){
        $shopIds =array();
        $shops =   DB::table('seller_services')->select('shop_id')
            ->whereIn('subservice_id',(array)DB::table('proposals')->select('services')->where('perposal_id',$proposal_id)->first())->get();
            foreach($shops as $shopsId){
                    $shopIds[] = $shopsId->shop_id;
            }
        $shopDetails = DB::table('shop')->whereIn('id',$shopIds)->get();
            
        $data = array("shops"=>$shopDetails, "proposal"=>$proposal_id);
        return view('inviteprovider')->with($data);
    }

    function receive_invitation(){
       $userId =  Auth::user()->id;
        $proposals = DB::table('proposals')
                    ->leftJoin('sent_invitations','sent_invitations.proposal_Id','=','proposals.perposal_id')
                    ->leftJoin('users','users.id','=','sent_invitations.sender_id')
                    ->Where('sent_invitations.receiver_id',$userId)
                    ->select('proposals.title','proposals.description','proposals.start_date','proposals.end_date','users.fname as senderfname','users.lname as senderlname','sent_invitations.sender_id','sent_invitations.status','sent_invitations.created_at','sent_invitations.invite_id')
                    ->get();
                    
        $serviceProvider = DB::table('users')
                            ->leftJoin('shop','shop.user_id','=','users.id')->where('users.id',$userId)->first();
        $data = array("invitations"=>$proposals,'company'=>$serviceProvider);
        return view('receivedinvitation')->with($data);
    
       
    }

    function invitation_action($action,$id){
        switch($action):
            case "decline":
                    DB::table('sent_invitations')->where('invite_id',$id)->update(['status'=>2]);
                break;
            default:
            DB::table('sent_invitations')->where('invite_id',$id)->update(['status'=>0]);
        endswitch;
      return  redirect('/invitation');

    }

    function view_sent_invitations($id){
            $userId =  Auth::user()->id;
            $proposals = DB::table('proposals')
                    ->leftJoin('sent_invitations','sent_invitations.proposal_Id','=','proposals.perposal_id')
                    ->leftJoin('shop','shop.user_id','=','sent_invitations.receiver_id')
                    ->Where('sent_invitations.sender_id',$userId)
                    ->Where('sent_invitations.proposal_Id',$id)
                    ->select('proposals.title','proposals.description','shop.shop_name','sent_invitations.sender_id','sent_invitations.status','sent_invitations.created_at','sent_invitations.invite_id')
                    ->get();
            $data = array("invitations"=>$proposals);
            return view('sentinvitation')->with($data);
    }

    function single_RFP($id){
        $userId =  Auth::user()->id;
        $proposals = DB::table('proposals')
                    ->leftJoin('sent_invitations','sent_invitations.proposal_Id','=','proposals.perposal_id')
                    ->leftJoin('users','users.id','=','sent_invitations.sender_id')
                    ->Where('sent_invitations.invite_id',$id)
                    ->select('proposals.title','proposals.description','proposals.start_date','proposals.end_date','users.fname as senderfname','users.lname as senderlname','sent_invitations.sender_id','sent_invitations.status','sent_invitations.created_at','sent_invitations.invite_id')
                    ->first();
                    
        $serviceProvider = DB::table('users')
                            ->leftJoin('shop','shop.user_id','=','users.id')->where('users.id',$userId)->first();
        $data = array("invitation"=>$proposals,'company'=>$serviceProvider);
        return view('proposalrespondform')->with($data);
    }


    function chat_window($id){
        $userId =  Auth::user()->id;
        $proposals = DB::table('proposals')
                    ->leftJoin('sent_invitations','sent_invitations.proposal_Id','=','proposals.perposal_id')
                    ->leftJoin('users','users.id','=','sent_invitations.sender_id')
                    ->Where('sent_invitations.invite_id',$id)
                    ->select('proposals.title','proposals.description','proposals.start_date','proposals.end_date','users.fname as senderfname','users.lname as senderlname','sent_invitations.sender_id','sent_invitations.status','sent_invitations.created_at','sent_invitations.invite_id')
                    ->first();
                    
        $serviceProvider = DB::table('users')
                            ->leftJoin('shop','shop.user_id','=','users.id')->where('users.id',$userId)->first();
        $data = array("invitation"=>$proposals,'company'=>$serviceProvider);

        return view('chatwindow')->with($data);
    }

    function tamplateProposal(Request $res){
 
        $initations =  DB::table('sent_invitations')
                        ->leftJoin('proposals','proposals.perposal_id','=','sent_invitations.proposal_Id')->where('sent_invitations.invite_id',$res->inviteId)->first();
        $serviceProvider = DB::table('users')
                            ->leftJoin('shop','shop.user_id','=','users.id')->where('users.id',$initations->receiver_id)->first();
        $customer = DB::table('users')->where('id',$initations->sender_id)->first();
        $data = array("postdata"=>(Object)$res->all());
        $documentFile = "/documents/";
        $file = base_path('images'.$documentFile.time().'p.pdf');
        PDF::loadHTML(view('proposalrespoanse')->with($data))->setPaper('a4', 'landscape')->setWarnings(false)->save($file);


        Mail::send('proposalemailtmplate', ['shop_name' => $serviceProvider->shop_name, 'job_title' => $initations->title, 'desc' =>$initations->description, 'customer_name' => $customer->fname.' '.$customer->lname], 
        function ($message) use ($serviceProvider,$customer,$file)
        {
            $message->subject('Respond to proposal '.$serviceProvider->fname.' '.$serviceProvider->lname);
            $message->from('info@hometrumpeter.com', 'Hometrumpeter');
            $message->attach($file);
            $message->to($serviceProvider->email); 
        });

        if (Mail::failures()) {
            back()->with('error', 'Respond to proposal failed. Please try again.');
            return redirect('invitation');
        }else{
            DB::table('sent_invitations')->where('invite_id',$res->inviteId)->update(['status'=>1]);
            back()->with('success', 'Respond to proposal has been sent.');
            return redirect('invitation');
        }
    }

}
