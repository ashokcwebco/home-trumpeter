<?php

namespace Responsive\Http\Controllers\Auth;

use Responsive\User;
use Illuminate\Support\Facades\Input;
use Responsive\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;
use URL;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
     protected $redirectTo = 'thanku';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|regex:/^[\w-]*$/|unique:users|max:255',
            'fname' => 'required|regex:/^[\w-]*$/|max:255',
            'lname' => 'required|regex:/^[\w-]*$/|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
			'gender' => 'required|string|max:255',
			'usertype' => 'required|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $access_token =str_random(50);
        if($data['usertype'] ==2){
            $userObj =  User::create([
                'name' => $data['name'],
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'email' => $data['email'],
                'service_type' => $data['service_type'],
                'password' => bcrypt($data['password']),
                'gender' => $data['gender'],
                'phone' => $data['phoneno'],
                'verification_token' => $access_token,
                'photo' => '',
                'admin' => $data['usertype'],
            ]);
        }else{
            $userObj =  User::create([
                'name' => $data['name'],
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'gender' => $data['gender'],
                'phone' => $data['phoneno'],
                'verification_token' => $access_token,
                'photo' => '',
                'admin' => $data['usertype'],
            ]);
        }
        if($userObj){
            $link = URL::to('/register/'.$userObj['id'].'/'.$access_token);
            back()->with('success', 'Please verify account by we have sent a mail.');
            Mail::send('registermail', ['fname' => ucfirst($data['fname']), 'lname' => ucfirst($data['lname']), 'link' =>$link], 
            function ($message)
            {
                $message->subject('Verify Registered account. ');
                $message->from('info@hometrumpeter.com', 'Hometrumpeter');
                $message->to(Input::get('email'));
            });
            return $userObj;
        }
    }

}
