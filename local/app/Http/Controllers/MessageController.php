<?php

namespace Responsive\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Mail;
use Auth;
use File;
use Image;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    

    function save_message(Request $req){

        $userId = Auth::user()->id;
        // $invitation =  DB::table('sent_invitations')->where('invite_id',$req->inviteId)->first();
            // DB::table('rfp_discussion')->where('invite_id',$req->inviteId)->where('user_id','!=',$userId)->update(['status'=>2]);
       if($inserted = DB::table('rfp_messages')->insertGetId(['message'=>$req->msg,'type'=>'text','extension'=>'','status'=>0,'created_at'=>date('Y-m-d H:i:s')])){
            $insert =   DB::table('rfp_discussion')->insertGetId(['user_Id'=>$userId,'invite_id'=>$req->inviteId,'message_id'=>$inserted,'status'=>1,'created_at'=>date('Y-m-d H:i:s')]);
       }else{
        $insert = 0;
       }
       DB::table('rfp_discussion')->where('invite_id',$req->inviteId)->where('user_id','!=',$userId)->update(['status'=>2]);
       $msg =  DB::table('rfp_discussion')->select('rfp_discussion.*','rfp_messages.message','rfp_messages.status as is_seen')
        ->leftJoin('rfp_messages','rfp_messages.msg_id','=','rfp_discussion.message_id')
        ->where(function ($query) use($req){
            $query->where('rfp_discussion.invite_id', $req->inviteId)
                ->where('rfp_discussion.status', 1)
                ->where('rfp_discussion.user_Id','!=', Auth::user()->id);
        })->orWhere(function($query) use($req,$insert){
            $query->where('rfp_discussion.invite_id', $req->inviteId)
                ->where('rfp_discussion.message_id', $insert);	
        })
        ->get();
        

       echo json_encode(array('data'=>$msg));
    }


    function get_message(Request $req){
        $userId = Auth::user()->id;
        DB::table('rfp_discussion')->where('invite_id',$req->inviteId)->where('user_id','!=',$userId)->update(['status'=>2]);
        $msg =  DB::table('rfp_discussion')->select('rfp_discussion.*','rfp_messages.message','rfp_messages.status as is_seen')
        ->leftJoin('rfp_messages','rfp_messages.msg_id','=','rfp_discussion.message_id')
        ->where('rfp_discussion.invite_id',$req->inviteId)
        ->orderBy('rfp_messages.msg_id', 'DESC')
        ->limit(10)
        ->get();
        echo json_encode(array('data'=>$msg));
    }


    function loadDataInterval(Request $req){
        $msg =  DB::table('rfp_discussion')->select('rfp_discussion.*','rfp_messages.message','rfp_messages.status as is_seen')
        ->leftJoin('rfp_messages','rfp_messages.msg_id','=','rfp_discussion.message_id')
        ->where('rfp_discussion.status', 1)
        ->where('rfp_discussion.user_Id','!=', Auth::user()->id)
        ->whereIn('rfp_discussion.invite_id', [$req->inviteId])
        ->get();
        DB::table('rfp_discussion')->where('invite_id',$req->inviteId)->where('user_id','!=',Auth::user()->id)->update(['status'=>2]);
        echo json_encode(array('data'=>$msg));
    }

    function uploadImage(Request $req){
        $documentFile = "/discussFile/";
        $file = $req->photos;
        $Extension = $file->getClientOriginalExtension();
        $filename = time() . '.' .$Extension;
        $path = base_path('images' . $documentFile);
        if($file->move($path, $filename)){
            $userId = Auth::user()->id;
            if($inserted = DB::table('rfp_messages')->insertGetId(['message'=>$filename,'type'=>'file','extension'=>$Extension,'status'=>0,'created_at'=>date('Y-m-d H:i:s')])){
                    $insert =   DB::table('rfp_discussion')->insertGetId(['user_Id'=>$userId,'invite_id'=>$req->inviteId,'message_id'=>$inserted,'status'=>1,'created_at'=>date('Y-m-d H:i:s')]);
            }else{
                $insert = 0;
            }

            $msg =  DB::table('rfp_discussion')->select('rfp_discussion.*','rfp_messages.message','rfp_messages.status as is_seen')
                ->leftJoin('rfp_messages','rfp_messages.msg_id','=','rfp_discussion.message_id')
                ->where(function ($query) use($req){
                    $query->where('rfp_discussion.invite_id', $req->inviteId)
                        ->where('rfp_discussion.status', 1)
                        ->where('rfp_discussion.user_Id','!=', Auth::user()->id);
                })->orWhere(function($query) use($req,$insert){
                    $query->where('rfp_discussion.invite_id', $req->inviteId)
                        ->where('rfp_discussion.message_id', $insert);	
                })
                ->get();
                DB::table('rfp_discussion')->where('invite_id',$req->inviteId)->where('user_id','!=',$userId)->update(['status'=>2]);

            echo json_encode(array('data'=>$msg));
        }
    }
}
