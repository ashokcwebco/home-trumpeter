<?php
namespace Responsive\Http\Controllers;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use File;
use Image;
class DashboardController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        $userid = Auth::user()->id;
        $editprofile = DB::table('users')->where('id', $userid)->first();
        if ($editprofile->admin == 1)
            $editprofile->userTypeTitle = 'Admin';
        elseif ($editprofile->admin == 2)
            $editprofile->userTypeTitle = "Seller";
        else
            $editprofile->userTypeTitle = "Customer";
		$editIdentityProof = DB::table('documents')->where('user_id', $userid)->first();
        $data = array('editprofile' => $editprofile, 'proof' => $editIdentityProof);
        return view('dashboard')->with($data);
	}
	
    public function sangvish_logout() {
        Auth::logout();
        return back();
	}
	
    public function sangvish_deleteaccount() {
        $userid = Auth::user()->id;
        $userdetails = DB::table('users')
                ->where('id', '=', $userid)
                ->get();
        $getshop = DB::table('shop')
                ->where('user_id', '=', $userid)
                ->get();
        $getshop_count = DB::table('shop')
                ->where('user_id', '=', $userid)
                ->count();
        $uemail = $userdetails[0]->email;
        DB::delete('delete from seller_services where user_id = ?', [$userid]);
        DB::delete('delete from rating where email = ?', [$uemail]);
        if (!empty($getshop_count)) {
            $shopid = $getshop[0]->id;
            DB::delete('delete from booking where shop_id = ?', [$shopid]);
            DB::delete('delete from withdraw where withdraw_shop_id = ?', [$shopid]);
        }
        DB::delete('delete from shop_gallery where user_id = ?', [$userid]);
        DB::delete('delete from shop where user_id = ?', [$userid]);
        DB::delete('delete from users where id!=1 and id = ?', [$userid]);
        return back();
	}
	

    protected function sangvish_edituserdata(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $rules = array(
            'email' => 'required|email|unique:users,email,' . $request->id,
            'name' => 'required|regex:/^[\w-]*$/|max:255|unique:users,name,' . $request->id,
            'photo' => 'max:1024|mimes:jpg,jpeg,png'
        );
        $messages = array(
            'email' => 'The :attribute field is already exists',
            'name' => 'The :attribute field must only be letters and numbers (no spaces)'
        );
        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $failedRules = $validator->failed();
            return back()->withErrors($validator);
        } else {
            $image = Input::file('photo');
            if ($image != "") {
                $userphoto = "/userphoto/";
                $delpath = base_path('images' . $userphoto . $request->currentphoto);
                File::delete($delpath);
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = base_path('images' . $userphoto . $filename);
                Image::make($image->getRealPath())->resize(200, 200)->save($path);
                $savefname = $filename;
            } else
                $savefname = $request->currentphoto;
            if ($request->password != "")
                $passtxt = bcrypt($request->password);
            else
                $passtxt = $request->savepassword;
            $data = array(
                "name" => $request->name,
                "gender" => $request->gender,
                "email" => $request->email,
                "phone" => $request->phone,
                "photo" => $request->savefname,
                "admin" => $request->usertype,
            );
            DB::table('users')->where('id', $request->id)->update($data);
            DB::update('update shop set seller_email="' . $request->email . '" where user_id = ?', [$request->id]);
            return back()->with('success', 'Account has been updated');
        }
	}
	
    function edit_document(Request $request) {
        $idProof = Input::file('idProof');
        $addressProof = Input::file('addressProof');
        $documentFile = "/documents/";
        $trade = '';
        $proofInsurance = '';
        if ($idProof != "") {
            $delpath = base_path('images' . $documentFile . $request->currentIdProof);
            File::delete($delpath);
            $trade = time() . '.' . $idProof->getClientOriginalExtension();
            $path = base_path('images' . $documentFile . $trade);
            Image::make($idProof->getRealPath())->save($path);
        }
        if ($addressProof != "") {
            $delpath = base_path('images' . $documentFile . $request->currentAddressProof);
            File::delete($delpath);
            $proofInsurance = time() . '.' . $addressProof->getClientOriginalExtension();
            $path = base_path('images' . $documentFile . $proofInsurance);
            Image::make($addressProof->getRealPath())->save($path);
        }
        $trade = $trade ? $trade : $request->currentIdProof;
        $proofInsurance = $proofInsurance ? $proofInsurance : $request->currentAddressProof;
        DB::table('documents')->where('doc_id', $request->doc_id)->update(['trade' => $trade, 'proofInsurance' => $proofInsurance]);
        DB::update('update users set service_type =' . $request->serviceType . ' where id =' . Auth::user()->id);
        return back()->with('success', 'Document has been updated.');
    }
}
