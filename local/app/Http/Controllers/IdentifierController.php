<?php

namespace Responsive\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;
use File;
use Image;
use URL;
use Mail;
use Illuminate\Support\Facades\Validator;
class IdentifierController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }
    function identify_user(Request $request) {
        $userid = Auth::user()->id;
        $userData = DB::table('users')->where('id',$userid)->first();


        // echo '<pre>';
        // print_r($userData);die;
        $services = DB::table('services')->get();
        $sub_services = DB::table('subservices')->get();
        if($request->id == Auth::user()->id && Auth::user()->status == 0 && $request->token){
            DB::table('users')
            ->where('verification_token',$request->token)
            ->where('id',$userid)
            ->update(['status'=>1]);
            $data = array('user_id' => $userid, 'userData' => $userData, 'services' =>$services,'subServices'=>$sub_services);
           return view('auth/register')->with($data);
        }else if(Auth::user()->status ==1){
            $document = DB::table('documents')->where('user_id', '=', $userid)->first();
            if (empty($document) && $userData->admin == 2) {
                $data = array('user_id' => $userid, 'userData' => $userData, 'services' =>$services,'subServices'=>$sub_services);
                return view('auth/register')->with($data);
            }else{
                return redirect('dashboard');
            }
        }else{
            $data = array('userData' => $userData);
            return view('auth/register')->with($data);
        }
    }

    function upload_document(Request $request) {
        $trade = Input::file('trade');
        $proofInsurance = Input::file('proofInsurance');

        $documentFile = "/documents/";
        $userphoto="/userphoto/";
        if ($trade != "") {

            $filename = time() . '.' . $trade->getClientOriginalExtension();
            $path = base_path('images' . $documentFile . $filename);
            Image::make($trade->getRealPath())->save($path);
        }

        if ($proofInsurance != "") {
            $addresFilename = time() . '.' . $proofInsurance->getClientOriginalExtension();
            $path = base_path('images' . $documentFile . $addresFilename);
            Image::make($proofInsurance->getRealPath())->save($path);
        }

        $profileImage = Input::file('photo');
        if($profileImage!="")
		{	
            $profilephoto = time() . '.' . $profileImage->getClientOriginalExtension();
			$path = base_path('images'.$userphoto.$profilephoto);
            Image::make($profileImage->getRealPath())->resize(200, 200)->save($path);
            $savefname=$profilephoto;
		}
        DB::table('documents')->insert(['user_id' =>Auth::user()->id, 'trade' => $filename, 'proofInsurance'=> $addresFilename]);
        DB::table('users')->where('id',Auth::user()->id)->update(['photo'=>$savefname]);

        return redirect('dashboard');
    }


    function updateUserProfile(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);
        $data = $request->all();
        $id = $data['id'];
        $input['email'] = Input::get('email');
        $input['name'] = Input::get('name');
        $rules = array(
            'email' => 'required|email|unique:users,email,' . $id,
            'name' => 'required|regex:/^[\w-]*$/|max:255|unique:users,name,' . $id,
        );


        $messages = array(
            'email' => 'The :attribute field is already exists',
            'name' => 'The :attribute field must only be letters and numbers (no spaces)'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $failedRules = $validator->failed();
            return back()->withErrors($validator);
        } else {
            $name = $data['name'];
            $email = $data['email'];
            $password = bcrypt($data['password']);

            $phone = $data['phoneno'];

            if ($data['password'] != "") {
                $passtxt = $password;
            } else {
                $passtxt = $data['savepassword'];
            }
            $admin = $data['usertype'];
            DB::table('users')
            ->where('id',$id)
            ->update(['fname'=>Input::get('fname'),'lname'=>Input::get('lname'),'phone'=>$phone, 'password'=>$passtxt,'service_type'=>Input::get('service_type'),'gender'=>Input::get('gender')]);

            return back()->with('success', 'Account has been updated');
        }
    }

    function thanku(){
        return view('registerthanku');
    }

}
