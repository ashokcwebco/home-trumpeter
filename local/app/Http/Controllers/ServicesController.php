<?php

namespace Responsive\Http\Controllers;



use File;
use Image;
use Responsive\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;

use Responsive\Http\Requests;
use Illuminate\Http\Request;
use Responsive\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
	public function __construct()
    {
        $this->middleware('auth');
    }
 
	 
    public function sangvish_index()
    {
        $services = DB::table('services')->orderBy('name', 'asc')->get();
		
		$set_id=1;
		$setting = DB::table('settings')->where('id', $set_id)->get();
		
		$uuid=Auth::user()->id;
		$uid=Auth::user()->email;
		$shopview = DB::table('shop')->where('seller_email', $uid)->get();
		$serviceType = DB::table('service_type')->get();
		$conditions = DB::table('conditions')->get();
		$viewservice = DB::table('seller_services')
		->where('user_id', $uuid)
		->orderBy('id','desc')
		->leftJoin('subservices', 'subservices.subid', '=', 'seller_services.subservice_id')
		->get();
		$data = array('services' => $services, 'setting' => $setting, 'serviceTypes'=>$serviceType, 'conditions'=>$conditions, 'shopview' => $shopview, 'uuid' => $uuid, 'viewservice' => $viewservice, 'editid' => '');

        return view('services')->with($data); 
		  
    }
	
	
	public function sangvish_destroy($did) {
		
		
      DB::delete('delete from seller_services where id = ?',[$did]);
	   
      
	 
	  return redirect('services');
      
   }
   
   
   public function sangvish_editdata($id) {
       $services = DB::table('services')->orderBy('name', 'asc')->get();
	   $set_id=1;
		$setting = DB::table('settings')->where('id', $set_id)->get();
		$serviceType = DB::table('service_type')->get();
		$conditions = DB::table('conditions')->get();
		$uuid=Auth::user()->id;
		$uid=Auth::user()->email;
		
		$shopview = DB::table('shop')->where('seller_email', $uid)->get();
		
		$viewservice = DB::table('seller_services')
		->where('user_id', $uuid)
		->orderBy('id','desc')
		->leftJoin('subservices', 'subservices.subid', '=', 'seller_services.subservice_id')
		->get();
		
		
		$sellservices = DB::table('seller_services')->where('id',$id)->first();
		$editid=$id;
	   
      $data = array('services' => $services, 'setting' => $setting, 'serviceTypes'=>$serviceType, 'conditions'=>$conditions, 'shopview' => $shopview, 'uuid' => $uuid, 'viewservice' => $viewservice , 'sellservices' => $sellservices,
	  'editid' => $editid);

        return view('services')->with($data); 
   }
   
   
   protected function sangvish_savedata(Request $request)
   {
	   $servi_id=DB::table('subservices')->where('subid', $request->get('subservice_id'))->get();
	   $service_id = $servi_id[0]->service;
	   $servicecnt = DB::table('seller_services')
				->where('user_id', '=', $request->get('user_id'))
				->where('shop_id', '=', $request->get('shop_id'))
				->where('subservice_id', '=', $request->get('subservice_id'))
				->count();
	   $data = 	array(
					'service_id'=>$service_id,
					'subservice_id'=>$request->get('subservice_id'),
					'price'=>$request->get('price'),
					'time'=>$request->get('time'),
					'user_id'=>$request->get('user_id'),
					'shop_id'=>$request->get('shop_id'),
					'service_type'=>$request->get('serviceType'),
					'conditions' =>$request->get('condition')
	   );
	   if(!$request->get('editid'))
	   {
			if($servicecnt==0){
				DB::table('seller_services')->insert($data);
				return back()->with('success', 'Services has been added');
			}else{
				return back()->with('error','That services is already added.');
			}
	   }else {  
		   $editid = $request->get('editid');
		   DB::table('seller_services')->where('id',$editid)->update($data);
			return back()->with('success', 'Services has been updated');
	   }		   
	   
   }
   
   
	
}