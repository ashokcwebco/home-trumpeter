<?php
namespace Responsive\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Mail;
use Auth;
class MybookingsController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }
    public function sangvish_showpage() {
        $email = Auth::user()->email;
        $set_id = 1;
        $setting = DB::table('settings')->where('id', $set_id)->get();
        $booking = DB::table('booking')
                ->leftJoin('shop', 'shop.id', '=', 'booking.shop_id')
                ->leftJoin('users', 'users.email', '=', 'shop.seller_email')
                ->where('booking.user_email', '=', $email)
                ->where('booking.status', '=', 'paid')
                ->where('shop.status', '=', 'approved')
                ->orderBy('booking.book_id', 'desc')
                /* ->groupBy('booking.shop_id') */
                ->get();
        $count = DB::table('booking')
                ->leftJoin('shop', 'shop.id', '=', 'booking.shop_id')
                ->leftJoin('users', 'users.email', '=', 'shop.seller_email')
                ->where('booking.user_email', '=', $email)
                ->where('booking.status', '=', 'paid')
                ->where('shop.status', '=', 'approved')
                ->orderBy('booking.book_id', 'desc')
                ->groupBy('booking.shop_id')
                ->count();
        $data = array('booking' => $booking, 'count' => $count, 'setting' => $setting, 'user_id' => Auth::user()->id);
        return view('my_bookings')->with($data);
    }
    public function sangvish_savedata(Request $request) {
            DB::table('rating')->insert(['rating'=>$request->rating,'user_id'=>Auth::user()->id, 'rbook_id'=>$request->book_id, 'rshop_id'=>$request->shop_id,'comment'=>$request->comment]);
            return redirect()->back()->with('message', 'Your Comments Added Successfully.');
    }
    public function destroy($id) {
        DB::delete('delete from booking where book_id = ?', [$id]);
        return back();
	}
	
	function complete_booking($id){
		echo DB::table('booking')->where('book_id',$id)->update(['is_completed'=>1]);
    }
    
    function dispute_booking(Request $req){
        DB::table('dispute')->insert(['user_Id'=>Auth::user()->id,'book_id'=>$req->book_id,'message'=>$req->message, 'created_at'=>date('Y-m-d H:i:s'), 'status'=>0 ]);
        DB::table('booking')->where('book_id',$req->book_id)->update(['is_completed'=>2]);
        return redirect('my_bookings');
	}
}
